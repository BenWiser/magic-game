const luabundle = require('luabundle');
const fs = require('fs');
const path = require('path');
const process = require('process');

process.chdir(path.resolve(process.cwd(), 'std', 'src'));
const bundledLua = luabundle.bundle(path.resolve(__dirname, 'src', 'Runtime.lua'));

process.chdir(path.resolve(process.cwd(), '..'));
fs.writeFileSync(path.resolve(__dirname, 'dist', 'bundled.lua'), bundledLua);
local ComponentManager = require('std.ComponentManager')

local Component = {
    initialise = function(entity) end,
    update = function(entity) end,
}

function Component:new(name, o)
    local component = o or {}
    setmetatable(component, {
        __index = self,
    })
    component.name = name
    ComponentManager.addComponentTemplate(component)
    return component
end

function Component:create()
    local component = setmetatable({}, getmetatable(self))
    for key, value in pairs(self) do
        component[key] = value
    end
    return component
end

return Component
local Entity = require('std.Entity')

local Tile = {}

function Tile:new(name, o)
    o.isStatic = true
    o._isTile = true
    Entity:new(name, o)
end

return Tile
local TableSerialiser = require('std.TableSerialiser')

local function serialise(entityTemplate)
    return TableSerialiser.serialise({
        animationManager = {
            rows = entityTemplate.animationManager.rows,
            columns = entityTemplate.animationManager.columns,
            animations = entityTemplate.animationManager.animations
        }
    })
end

local function deserialise(entityTemplate, json)
    local deserialised = TableSerialiser.deserialise(json)
    entityTemplate.animationManager.rows = deserialised.animationManager.rows
    entityTemplate.animationManager.columns = deserialised.animationManager.columns
    entityTemplate.animationManager.animations = deserialised.animationManager.animations
end

return {
    serialise = serialise,
    deserialise = deserialise
}
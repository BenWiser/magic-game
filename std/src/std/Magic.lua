return {
    create_texture = function (fileName)
        return Magic_create_texture(fileName)
    end,
    destroy_texture = function (textureId)
        return Magic_destroy_texture(textureId)
    end,
    render_rect_colour = function (red, green, blue, x, y, width, height)
        return Magic_render_rect_colour(red, green, blue, x, y, width, height)
    end,
    render_rect = function (textureId, x, y, width, height)
        return Magic_render_rect(textureId, x, y, width, height)
    end,
    render_spritemap_frame = function (textureId, maxFrames, animations, frame, animation, x, y, width, height, angle)
        return Magic_render_spritemap_frame(textureId, maxFrames, animations, frame, animation, x, y, width, height, angle)
    end,
    render_text = function (text, fontPath, x, y, fontSize)
        return Magic_render_text(text, fontPath, x, y, fontSize)
    end,
    render_set_depth = function (depth)
        return Magic_render_set_depth(depth)
    end,
    render_set_alpha = function (alpha)
        return Magic_render_set_alpha(alpha)
    end,
    render_set_tiling = function (horizontalTiling, verticalTiling)
        return Magic_render_set_tiling(horizontalTiling, verticalTiling)
    end,
    render_shader_create = function (vertexShaderSource, fragmentShaderSource)
        return Magic_render_shader_create(vertexShaderSource, fragmentShaderSource)
    end,
    render_shader_set = function (shaderId)
        return Magic_render_shader_set(shaderId)
    end,
    render_shader_set_default = function ()
        return Magic_render_shader_set_default()
    end,
    iterate_physics = function ()
        return Magic_iterate_physics()
    end,
    set_physics_gravity = function (forceX, forceY)
        return Magic_set_physics_gravity(forceX, forceY)
    end,
    create_physics_body = function (x, y, width, height, friction, density, isDynamic, fixedRotation, isSensor)
        return Magic_create_physics_body(x, y, width, height, friction, density, isDynamic, fixedRotation, isSensor)
    end,
    apply_force_to_physics_body = function (bodyId, forceX, forceY)
        return Magic_apply_force_to_physics_body(bodyId, forceX, forceY)
    end,
    apply_impulse_to_physics_body = function (bodyId, forceX, forceY)
        return Magic_apply_impulse_to_physics_body(bodyId, forceX, forceY)
    end,
    apply_angular_impulse_to_physics_body = function (bodyId, angle)
        return Magic_apply_angular_impulse_to_physics_body(bodyId, angle)
    end,
    destroy_physics_body = function (bodyId)
        return Magic_destroy_physics_body(bodyId)
    end,
    get_physics_body_velocity = function (bodyID)
        return Magic_get_physics_body_velocity(bodyID)
    end,
    get_physics_body_position = function (bodyId)
        return Magic_get_physics_body_position(bodyId)
    end,
    set_physics_body_velocity = function(bodyId, forceX, forceY)
        return Magic_set_physics_body_velocity(bodyId, forceX, forceY)
    end,
    set_physics_body_position = function (bodyId, x, y, rotation)
        return Magic_set_physics_body_position(bodyId, x, y, rotation)
    end,
    physics_has_collided = function (bodyIdA, bodyIdA)
        return Magic_physics_has_collided(bodyIdA, bodyIdA)
    end,
    is_key_pressed = function (keyCode)
        return Magic_is_key_pressed(keyCode)
    end,
    is_mouse_button_pressed = function (mouseButton)
        return Magic_is_mouse_button_pressed(mouseButton)
    end,
    get_mouse_position = function ()
        return Magic_get_mouse_position()
    end,
    get_mouse_scroll = function ()
        return Magic_get_mouse_scroll()
    end,
    ray_trace = function (x1, y1, x2, y2)
        return Magic_ray_trace(x1, y1, x2, y2)
    end,
    query_bounds = function (x1, y1, x2, y2)
        return Magic_query_bounds(x1, y1, x2, y2)
    end,
    get_screen_dimensions = function ()
        return Magic_get_screen_dimensions()
    end,
    move_camera = function (x, y, zoom)
        return Magic_move_camera(x, y, zoom)
    end,
    get_camera = function ()
        return Magic_get_camera()
    end,
    file_write = function (fileName, data)
        return Magic_file_write(fileName, data)
    end,
    file_read = function (fileName)
        return Magic_file_read(fileName)
    end,
    sound_load = function (fileName)
        return Magic_sound_load(fileName)
    end,
    sound_play = function (soundId)
        return Magic_sound_play(soundId)
    end,
    physics_query_collisions = function (physicsEngineId)
        return Magic_physics_query_collisions(physicsEngineId)
    end,
}
local Magic = require('std.Magic')

local function queryCollisions(physicsEngineId)
    return Magic.physics_query_collisions(physicsEngineId)
end

local function moveHorizontal(entity, forceX)
    local _, forceY = Magic.get_physics_body_velocity(entity.physicsEngineId)
    Magic.set_physics_body_velocity(entity.physicsEngineId, forceX, forceY)
end

local function moveVertical(entity, forceY)
    local forceX, _ = Magic.get_physics_body_velocity(entity.physicsEngineId)
    Magic.set_physics_body_velocity(entity.physicsEngineId, forceX, forceY)
end

local function move(entity, forceX, forceY)
    Magic.set_physics_body_velocity(entity.physicsEngineId, forceX, forceY)
end

return {
    queryCollisions = queryCollisions,
    moveHorizontal = moveHorizontal,
    moveVertical = moveVertical,
    move = move,
}
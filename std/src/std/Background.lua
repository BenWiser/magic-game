local Magic = require('std.Magic')
local GameSettings = require('std.GameSettings')

function renderStatic(textureId)
    Magic.render_rect(
        textureId,
        GameSettings.GAME_HALF_WIDTH,
        GameSettings.GAME_HALF_HEIGHT,
        GameSettings.GAME_WIDTH,
        GameSettings.GAME_HEIGHT
    )
end

function renderParallax(textureId, depth)
    depth = depth or 1.2

    local cameraX, cameraY = Magic.get_camera()

    local distanceX = math.abs(cameraX - GameSettings.GAME_WIDTH) / GameSettings.GAME_WIDTH
    local extraWidth = (
        GameSettings.GAME_WIDTH * depth
        - GameSettings.GAME_WIDTH
    ) * distanceX * (cameraX > GameSettings.GAME_WIDTH and -1 or 1)

    local distanceY = math.abs(cameraY - GameSettings.GAME_HEIGHT) / GameSettings.GAME_HEIGHT
    local extraHeight = (
        GameSettings.GAME_HEIGHT * depth
        - GameSettings.GAME_HEIGHT
    ) * distanceY * (cameraY > GameSettings.GAME_HEIGHT and -1 or 1)

    Magic.render_rect(
        textureId,
        GameSettings.GAME_HALF_WIDTH - extraWidth,
        GameSettings.GAME_HALF_HEIGHT - extraHeight,
        GameSettings.GAME_WIDTH * depth,
        GameSettings.GAME_HEIGHT * depth
    )
end

return {
    renderStatic = renderStatic,
    renderParallax = renderParallax
}
local luna = require('lib.lunajson.luna')


local function serialise(table)
    return luna.encode(table)
end

local function deserialise(str)
    return luna.decode(str)
end

return {
    serialise = serialise,
    deserialise = deserialise,
}
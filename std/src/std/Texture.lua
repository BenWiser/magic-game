local Magic = require('std.Magic')

local TEXTURES = {}

local Texture = {}

function Texture:new(filePath)
    local textureId = Magic.create_texture(filePath)
    table.insert(TEXTURES, textureId)
    return textureId
end

function Texture:cleanUpTextures()
    for textureId = 1, #TEXTURES do
        Magic.destroy_texture(TEXTURES[textureId])
        TEXTURES[textureId] = nil
    end
    TEXTURES = {}
end

return Texture
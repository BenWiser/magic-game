local Magic = require('std.Magic')

local screenX, screenY = Magic.get_screen_dimensions()

local GAME_WIDTH = screenX
local GAME_HEIGHT = screenY

local GameSettings = {
    GAME_WIDTH = GAME_WIDTH,
    GAME_HEIGHT = GAME_HEIGHT,
    GAME_HALF_WIDTH = GAME_WIDTH / 2,
    GAME_HALF_HEIGHT = GAME_HEIGHT / 2,
    PHYSICS_ENABLED = false,
}

function GameSettings:enablePhysics()
    GameSettings.PHYSICS_ENABLED = true
end

function GameSettings:disablePhysics()
    GameSettings.PHYSICS_ENABLED = false
end

function GameSettings:setGravity(forceX, forceY)
    Magic.set_physics_gravity(forceX, forceY)
end

function GameSettings:setGameDimensions(width, height)
    GameSettings.GAME_WIDTH = width
    GameSettings.GAME_HEIGHT = height
    GameSettings.GAME_HALF_WIDTH = width / 2
    GameSettings.GAME_HALF_HEIGHT = height / 2
end

return GameSettings

local AnimationManager = {
    frameIndex = 1.0,
    isPlaying = false,
    animationSpeed = 0.1,
    rows = 1,
    columns = 1,
    animations = {},
    currentAnimation = nil,
}

function AnimationManager:new(o)
    setmetatable(o, {
        __index = self
    })
    return o
end

function AnimationManager:isFinished()
    if self.animations[self.currentAnimation] == nil then
        return true
    end
    return self.frameIndex >= #(self.animations[self.currentAnimation]) + 1
end

function AnimationManager:startAnimation(animation, speed)
    if self.animations[animation] == nil then
        error('Invalid: Animation')
    end
    -- If the animation is already playing
    -- and hasn't finished, don't start it again
    if self.isPlaying and not self:isFinished() and animation == self.currentAnimation then
        return
    end
    self.animationSpeed = speed or self.animationSpeed
    self.currentAnimation = animation
    self.frameIndex = 1
    self.isPlaying = true
end

function AnimationManager:update()
    if not self.isPlaying or self:isFinished() then
        return
    end
    self.frameIndex = self.frameIndex + self.animationSpeed
end

function AnimationManager:getFrame()
    if self.currentAnimation == nil or self.animations[self.currentAnimation] == nil then
        return {0, 0}
    end
    return self.animations[self.currentAnimation][math.floor(self.frameIndex)]
end

return AnimationManager
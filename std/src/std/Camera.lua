local Magic = require('std.Magic')
local GameSettings = require('std.GameSettings')

local CAMERA_SPEED = 1.2

local function moveToWithCameraBounds(self, zoom)
    -- Calculate the limits of the camera
    local screenX, screenY = Magic.get_screen_dimensions()

    local left = ((screenX / 2) / zoom)
    local right = GameSettings.GAME_WIDTH - left
    local top = ((screenY / 2) / zoom)
    local bottom = GameSettings.GAME_HEIGHT - top

    local moveToX = math.min(math.max(self.x, left), right)
    local moveToY = math.min(math.max(self.y, top), bottom)

    return moveToX, moveToY
end

local function followEntity(self, zoom, smooth)
    local moveToX, moveToY = moveToWithCameraBounds(self, zoom)

    if smooth or false then
        local currentX, currentY = Magic.get_camera()
        local distance = math.abs(moveToY - currentY) + math.abs(moveToX - currentX)

        if distance == 0 then
            return
        end

        local angleBetween = math.atan2(moveToY - currentY, moveToX - currentX)
        moveToX = currentX + math.cos(angleBetween) * (CAMERA_SPEED * (distance / 10))
        moveToY = currentY + math.sin(angleBetween) * (CAMERA_SPEED * (distance / 10))
    end

    Magic.move_camera(
        moveToX,
        moveToY,
        zoom
    )
end

local function move(x, y, zoom)
    Magic.move_camera(
        x,
        y,
        zoom
    )
end

return {
    followEntity = followEntity,
    move = move,
}
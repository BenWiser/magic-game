local Magic = require('std.Magic')

local KEY_MAP = {
    right = 262,
    left = 263,
    down = 264,
    up = 265,
    action = 32,
}

local function mapKey(key)
    if KEY_MAP[key] ~= nil then
        return KEY_MAP[key]
    end
    return key
end

local function isPressed(key)
    local mappedKey = mapKey(key)
    return Magic.is_key_pressed(mappedKey)
end

return {
    isPressed = isPressed,
}
local Magic = require('std.Magic')

local EntityManager = require('std.EntityManager')
local TableSerialiser = require('std.TableSerialiser')

local SCENES = {}
local CURRENT_SCENE = {}

local function currentScene()
    return SCENES[CURRENT_SCENE]
end

local function saveSceneData(scene)
    local serialised = TableSerialiser.serialise({
        layers = scene._layers,
        maxEntityId = scene._maxEntityId,
        width = scene.width,
        height = scene.height,
    })
    Magic.file_write("./assets/levels/" .. scene.name .. ".json", serialised)
end

local function loadSceneData(scene)
    local serialised = Magic.file_read("./assets/levels/" .. scene.name .. ".json")
    if serialised ~= "" then
        local deserialised = TableSerialiser.deserialise(serialised)
        scene._layers = deserialised.layers
        scene._maxEntityId = deserialised.maxEntityId
        scene.width = deserialised.width
        scene.height = deserialised.height
    end
end

local function reloadScene()
    EntityManager.cleanUpEntities()
    local currentScene = SCENES[CURRENT_SCENE]
    if currentScene ~= nil then
        currentScene._create(currentScene)
    end
end

local function nextScene(currentScene)
    -- Make sure that the current scene is calling this
    if currentScene ~= CURRENT_SCENE then
        return
    end

    EntityManager.cleanUpEntities()
    CURRENT_SCENE = CURRENT_SCENE + 1

    local currentScene = SCENES[CURRENT_SCENE]
    if currentScene ~= nil then
        currentScene._create(currentScene)
    end
end

local function addScene(scene)
    loadSceneData(scene)
    table.insert(SCENES, scene)
    if #SCENES == 1 then
        CURRENT_SCENE = 1
        local scene = currentScene()
        scene._create(scene)
    end
    return #SCENES
end

local function changeScene(name)
    for i = 1, #SCENES do
        if SCENES[i].name == name then
            CURRENT_SCENE = i
            EntityManager.cleanUpEntities()
            local nextScene = currentScene()
            nextScene._create(nextScene)
            break
        end
    end
end

local function sceneIterator()
    local i = 0
    return function ()
        i = i + 1
        if i <= #SCENES then
            return SCENES[i]
        end
    end
end

local function getSceneNames()
    local sceneNames = {}
    local iterator = sceneIterator()
    local scene = iterator()
    while scene ~= nil do
        table.insert(sceneNames, scene.name)
        scene = iterator()
    end
    return sceneNames
end

return {
    addScene = addScene,
    reloadScene = reloadScene,
    nextScene = nextScene,
    currentScene = currentScene,
    changeScene = changeScene,
    getSceneNames = getSceneNames,
    saveSceneData = saveSceneData,
    loadSceneData = loadSceneData,
}
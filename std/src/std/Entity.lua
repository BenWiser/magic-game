local Magic = require('std.Magic')
local AnimationManager = require('std.AnimationManager')
local EntityManager = require('std.EntityManager')
local EntityTemplateSerialiser = require('std.EntityTemplateSerialiser')
local Physics = require('std.Physics')
local ComponentManager = require ('std.ComponentManager')

local Entity = {
    name = '',
    texture = 0,
    x = 0,
    y = 0,
    angle = 0,
    width = 0,
    height = 0,
    textureWidth = nil,
    textureHeight = nil,
    physicsEngineId = 0,
    friction = 0.8,
    density = 0.03,
    isStatic = false,
    isSensor = false,
    fixedRotation = false,
    alpha = 1.0,
    depth = 0.0,
    components = {},
    flippedHorizantal = false,
    tiling = {
        x = 1,
        y = 1
    },
    shader = nil,
    animationManager = nil,
    initialise = function(self) end,
    update = function(self) end
}

function Entity:new(name, o)
    setmetatable(o, {
        __index = self
    })
    o.animationManager = o.animationManager or AnimationManager:new {}
    o.name = name
    local json = Magic.file_read("./assets/entities/" .. name .. ".json")
    if json ~= "" then
        EntityTemplateSerialiser.deserialise(o, json)
    end
    EntityManager.addEntityTemplate(o)
    return o
end

function Entity:create(initialValues)
    -- Copy the "entity template"
    local entity = setmetatable({}, getmetatable(self))
    for key, value in pairs(self) do
        entity[key] = value
    end
    for key, value in pairs (initialValues) do
        entity[key] = value
    end

    entity.textureWidth = entity.textureWidth or entity.width
    entity.textureHeight = entity.textureHeight or entity.height

    entity.physicsEngineId = Magic.create_physics_body(
        entity.x,
        entity.y,
        entity.width,
        entity.height,
        entity.friction,
        entity.density,
        not entity.isStatic,
        entity.fixedRotation,
        entity.isSensor
    )
    Magic.set_physics_body_position(entity.physicsEngineId, entity.x, entity.y, entity.angle)
    entity.initialise(entity)
    if entity.components then
        for i = 1, #entity.components do
            entity.components[i].initialise(entity)
        end
    end
    return entity
end

function Entity:render()
    if self.texture == 0 then
        return
    end
    if self.shader ~= nil then
        Magic.render_shader_set(self.shader.shaderId)
    end
    local currentAnimation = self.animationManager:getFrame()
    Magic.render_set_tiling(self.tiling.x, self.tiling.y)
    Magic.render_set_alpha(self.alpha)
    Magic.render_set_depth(self.depth)
    Magic.render_spritemap_frame(
        self.texture,
        self.animationManager.columns,
        self.animationManager.rows,
        currentAnimation[1],
        currentAnimation[2],
        self.x,
        self.y,
        self.textureWidth * (self.flippedHorizantal and -1 or 1),
        self.textureHeight,
        self.angle
    )
    if self.shader ~= nil then
        Magic.Magic.render_shader_set_default()
    end
    Magic.render_set_depth(0)
    Magic.render_set_alpha(1.0)
    Magic.render_set_tiling(1.0, 1.0)
end

-- Before the entity can do anything, the physics body needs to be queried
function Entity:updatePhysics()
    if not self.isStatic then
        local x, y, angle = Magic.get_physics_body_position(self.physicsEngineId)
        self.x = x
        self.y = y
        self.angle = angle
    else
        Magic.set_physics_body_position(self.physicsEngineId, self.x, self.y, self.angle)
    end
end

function Entity:updateAnimations()
    self.animationManager:update()
end

function Entity:getComponent(componentName)
    if self.components then
        for i = 1, #self.components do
            if self.components[i].name == componentName then
                return self.components[i]
            end
        end
    end
    return nil
end

function Entity:hasComponent(componentName)
    local component = self:getComponent(componentName)
    return component ~= nil
end

function Entity:touchingComponent(componentName)
    local collisions = Physics.queryCollisions(self.physicsEngineId)

    local entityIterator = EntityManager.entityIterator()
    local entityB = entityIterator()
    while entityB ~= nil do
        for i = 1, #collisions do
            if entityB.physicsEngineId == collisions[i] then
                local component = entityB:getComponent(componentName)
                if component ~= nil then
                    return component
                end
            end
        end
        entityB = entityIterator()
    end

    return nil
end

function Entity:touchingEntity(entityName)
    local collisions = Physics.queryCollisions(self.physicsEngineId)

    local entityIterator = EntityManager.entityIterator()
    local entityB = entityIterator()
    while entityB ~= nil do
        for i = 1, #collisions do
            if entityB.physicsEngineId == collisions[i] and entityB.name == entityName then
                return entityB
            end
        end
        entityB = entityIterator()
    end

    return nil
end

function Entity:addComponent(componentName)
    table.insert(self.components, ComponentManager.createComponent(componentName))
end

return Entity
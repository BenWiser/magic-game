local EntityTemplateSerialiser = require('std.EntityTemplateSerialiser')
local Magic = require('std.Magic')

-- These are all the entities that can be created
local ENTITY_TEMPLATES = {}

-- These are all the entities that are in use
local ACTIVE_ENTITIES = {}
local ENTITIES_TO_DELETE = {}

-- Only needs to be called from entities
local function addEntityTemplate(entity)
    ENTITY_TEMPLATES[entity.name] = entity
end

-- Call this to create a new entity
local function create(entityName, initialValues)
    local entity = ENTITY_TEMPLATES[entityName]:create(initialValues or {})
    table.insert(ACTIVE_ENTITIES, entity)
    return entity
end

local function getTemplate(entityName)
    return ENTITY_TEMPLATES[entityName]
end

local function removeEntity(entity)
    table.insert(ENTITIES_TO_DELETE, entity)
end

-- Clear all active entities
local function cleanUpEntities()
    for entityIndex in pairs(ACTIVE_ENTITIES) do
        Magic.destroy_physics_body(ACTIVE_ENTITIES[entityIndex].physicsEngineId)
        ACTIVE_ENTITIES[entityIndex] = nil
    end

    ACTIVE_ENTITIES = {}
end

-- Go through all the entities to delete and clear them
-- Only needed for the runtime
local function removeDeletedEntities()
    for i in pairs(ENTITIES_TO_DELETE) do
        for entityIndex in pairs(ACTIVE_ENTITIES) do
            if ACTIVE_ENTITIES[entityIndex].physicsEngineId == ENTITIES_TO_DELETE[i].physicsEngineId then
                Magic.destroy_physics_body(ACTIVE_ENTITIES[entityIndex].physicsEngineId)
                table.remove(ACTIVE_ENTITIES, entityIndex)
            end
        end
    end

    ENTITIES_TO_DELETE = {}
end

local function getEntityTemplateNames()
    local names = {}
    for key in pairs(ENTITY_TEMPLATES) do
        table.insert(names, key)
    end
    return names
end

local function entityIterator()
    local i = 0
    return function ()
        i = i + 1
        if i <= #ACTIVE_ENTITIES then
            return ACTIVE_ENTITIES[i]
        end
    end
end

local function saveTemplates()
    for name, template in pairs(ENTITY_TEMPLATES) do
        if not template._isTile then
            local json = EntityTemplateSerialiser.serialise(template)
            Magic.file_write("./assets/entities/" .. name .. ".json", json)
        end
    end
end

return {
    create = create,
    addEntityTemplate = addEntityTemplate,
    removeEntity = removeEntity,
    cleanUpEntities = cleanUpEntities,
    removeDeletedEntities = removeDeletedEntities,
    getEntityTemplateNames = getEntityTemplateNames,
    entityIterator = entityIterator,
    getTemplate = getTemplate,
    saveTemplates = saveTemplates,
}
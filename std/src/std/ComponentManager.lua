local COMPONENT_TEMPLATES = {}

local function addComponentTemplate(component)
    COMPONENT_TEMPLATES[component.name] = component
end

local function getNames()
    local names = {}
    for name in pairs(COMPONENT_TEMPLATES) do
        table.insert(names, name)
    end
    return names
end

local function createComponent(name)
    return COMPONENT_TEMPLATES[name]:create()
end

local function createComponents(components)
    local componentsCreated = {}
    for name, isEnabled in pairs(components) do
        if isEnabled then
            local component = createComponent(name)
            table.insert(componentsCreated, component)
        end
    end
    return componentsCreated
end

return {
    addComponentTemplate = addComponentTemplate,
    getNames = getNames,
    createComponent = createComponent,
    createComponents = createComponents,
}
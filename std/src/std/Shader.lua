local Magic = require('std.Magic')

local Shader = {
    shaderId = -1
}

function Shader:new(shaderName)
    local o = {}

    setmetatable(o, {
        __index = self
    })

    local function readSource(type)
        return Magic.file_read("./assets/shaders/" .. shaderName .. "." .. type)
    end
    o.shaderId = Magic.render_shader_create(readSource("vert"), readSource("frag"))

    return o
end

return Shader
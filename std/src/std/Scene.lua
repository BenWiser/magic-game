local SceneManager = require('std.SceneManager')
local EntityManager = require('std.EntityManager')
local GameSettings = require('std.GameSettings')
local ComponentManager = require('std.ComponentManager')

-- A scene stores a level state

local Scene = {
    sceneId = 0,
    name = "",
    _layers = {{name = "layer1", entities = {}}},
    width = GameSettings.GAME_WIDTH,
    height = GameSettings.GAME_HEIGHT,
    initialise = function(scene) end,
    update = function(scene) end,
    _maxEntityId = 1,
    _addEntity = function(scene, layerIndex, entity)
        local layer = scene._layers[layerIndex]
        entity.entityId = scene._maxEntityId or 1
        scene._maxEntityId = scene._maxEntityId + 1
        table.insert(layer.entities, entity)
    end,
    _removeEntity = function (scene, layerIndex, entity)
        local layer = scene._layers[layerIndex]
        for i = 1, #layer.entities do
            if layer.entities[i].entityId == entity.entityId then
                table.remove(layer.entities, i)
                break
            end
        end
    end,
    _create = function(scene)
        for i = 1, #scene._layers do
            local layer = scene._layers[i]
            for l = 1, #layer.entities do
                local entity = layer.entities[l]
                local initialisation = {
                    scene = scene,
                    x = entity.x,
                    y = entity.y,
                    angle = entity.angle,
                    entityId = entity.entityId,
                    depth = entity.depth or i / 100,
                    alpha = entity.alpha or 1.0,
                    layer = i,
                    components = ComponentManager.createComponents(entity.components or {}),
                }

                if entity.tileData ~= nil then
                    initialisation.width = entity.tileData.width
                    initialisation.height = entity.tileData.height
                    initialisation.tiling = entity.tileData.tiling
                end
                EntityManager.create(entity.name, initialisation)
            end
            GameSettings:setGameDimensions(scene.width, scene.height)
            scene.initialise(scene)
        end
    end
}

function Scene:new(name, o)
    setmetatable(o, {
        __index = self
    })
    o.name = name
    o.sceneId = SceneManager.addScene(o)
    return o
end

return Scene
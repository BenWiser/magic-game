
-- This runtime is basically a basic bootstrap for the scripting layer
-- It manages details like which scene should be updated and when textures should be cleaned up
-- *NB: The general rule of thumb is that the game code should not be aware of the native API layer

-- Libs are provided for the std and are not meant to be publicly used outside of it
package.loaded['lib.lunajson.lunajson.sax'] = require('lib.lunajson.lunajson.sax')
package.loaded['lib.lunajson.lunajson.encoder'] = require('lib.lunajson.lunajson.encoder')
package.loaded['lib.lunajson.lunajson.decoder'] = require('lib.lunajson.lunajson.decoder')
package.loaded['lib.lunajson.luna'] = require('lib.lunajson.luna')

-- This is a cheeky hack to get the bundler to make the standard libraries available to the games
package.loaded['std.AnimationManager'] = require('std.AnimationManager')
package.loaded['std.Background'] = require('std.Background')
package.loaded['std.Camera'] = require('std.Camera')
package.loaded['std.Component'] = require('std.Component')
package.loaded['std.ComponentManager'] = require('std.ComponentManager')
package.loaded['std.Entity'] = require('std.Entity')
package.loaded['std.EntityManager'] = require('std.EntityManager')
package.loaded['std.EntityTemplateSerialiser'] = require('std.EntityTemplateSerialiser')
package.loaded['std.GameSettings'] = require('std.GameSettings')
package.loaded['std.Input'] = require('std.Input')
package.loaded['std.Magic'] = require('std.Magic')
package.loaded['std.Physics'] = require('std.Physics')
package.loaded['std.Scene'] = require('std.Scene')
package.loaded['std.SceneManager'] = require('std.SceneManager')
package.loaded['std.Shader'] = require('std.Shader')
package.loaded['std.TableSerialiser'] = require('std.TableSerialiser')
package.loaded['std.Texture'] = require('std.Texture')
package.loaded['std.Tile'] = require('std.Tile')

local EntityManager = require('std.EntityManager')
local GameSettings = require('std.GameSettings')
local SceneManager = require('std.SceneManager')
local Texture = require('std.Texture')

function print(str)
    Magic_print(str)
end

local function resetCamera()
    -- Reset the camera
    Magic_move_camera(
        GameSettings.GAME_HALF_WIDTH,
        GameSettings.GAME_HALF_HEIGHT,
        1
    )
end

-- This is the game loop that will be automatically executed each loop by the engine
function gameLoop()
    local currentScene = SceneManager.currentScene()
    if currentScene ~= nil then
        currentScene.update(currentScene)
    end
    EntityManager.removeDeletedEntities()
    local entityIterator = EntityManager.entityIterator()
    local entity = entityIterator()
    while entity ~= nil do
        if GameSettings.PHYSICS_ENABLED then
            entity:updatePhysics()
        end
        entity:updateAnimations()
        entity:update(entity)
        for i = 1, #entity.components do
            entity.components[i].update(entity)
        end
        entity:render()
        entity = entityIterator()
    end

    if GameSettings.PHYSICS_ENABLED then
        Magic_iterate_physics()
    end
end

-- This gets executed when the script engine is being cleaned up
function cleanUp()
    resetCamera()
    EntityManager.cleanUpEntities()
    Texture:cleanUpTextures()
end

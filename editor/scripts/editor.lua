-- Load the main game script so that we can inspect each level
require 'main'

local SceneEditor = require('editor.sceneEditor.Editor')
local AnimationEditor = require('editor.animationEditor.Editor')
local EditorState = require('editor.EditorState')
local Gui = require('editor.Gui')

local runtimeGameLoop = gameLoop

function gameLoop()
    Gui.render()

    if EditorState.editorMode == 'SCENE' then
        if EditorState.isRunning then
            runtimeGameLoop()
        else
            SceneEditor.render()
        end
    elseif EditorState.editorMode == 'ANIMATION_EDITOR' then
        AnimationEditor.render()
    end
end
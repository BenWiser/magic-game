local Texture = require('std.Texture')

local assetDirectory = Magic_editor_getAssetDirectory()

return {
    PLAY = Texture:new(assetDirectory .. "/textures/play.png"),
    PAUSE = Texture:new(assetDirectory .. "/textures/pause.png"),
    BACKGROUND = Texture:new(assetDirectory .. "/textures/background.png"),
    NEW = Texture:new(assetDirectory .. "/textures/new.png"),
    BIN = Texture:new(assetDirectory .. "/textures/bin.png"),
}
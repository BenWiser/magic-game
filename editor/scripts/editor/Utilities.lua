local SceneManager = require('std.SceneManager')
local Magic = require('std.Magic')
local EntityManager = require('std.EntityManager')

local EditorState = require('editor.EditorState')

local function getEntityDragged()
    if EditorState.entityDragged ~= -1 then
        local entityIterator = EntityManager.entityIterator()
        local entity = entityIterator()
        while entity ~= nil do
            if entity.physicsEngineId == EditorState.entityDragged then
                local layers = SceneManager.currentScene()._layers

                -- A bit of silly iteration to relate the physics entity back to
                -- the entity in the scene
                for i = 1, #layers do
                    local sceneEntities = layers[i].entities
                    for i = 1, #sceneEntities do
                        if sceneEntities[i].entityId == entity.entityId then
                            return sceneEntities[i], entity
                        end
                    end
                end
            end
            entity = entityIterator()
        end
    end
    return nil, nil
end

local function repositionEntity()
    local sceneEntity, physicsEngineEntity = getEntityDragged()
    if sceneEntity ~= nil and physicsEngineEntity ~= nil then
        sceneEntity.x = physicsEngineEntity.x
        sceneEntity.y = physicsEngineEntity.y
    end
end

local function save()
    local currentScene = SceneManager.currentScene()
    SceneManager.saveSceneData(currentScene)
    SceneManager.reloadScene()
end

local function isWithinCamera(x, y)
    local cameraX, cameraY, cameraZoom = Magic.get_camera()
    local gameWidth, gameHeight = Magic.get_screen_dimensions()

    return x > cameraX - (gameWidth / 2) / cameraZoom and x < cameraX + (gameWidth / 2) / cameraZoom and
        y > cameraY - (gameHeight / 2) / cameraZoom and y < cameraY + (gameHeight / 2) / cameraZoom
end

return {
    save = save,
    isWithinCamera = isWithinCamera,
    getEntityDragged = getEntityDragged,
    repositionEntity = repositionEntity,
}
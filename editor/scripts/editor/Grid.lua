local Grid = {
    GRID_SIZE = 32,
}

function Grid:coords(x, y)
    return x - x % self.GRID_SIZE, y - y % self.GRID_SIZE
end

return Grid
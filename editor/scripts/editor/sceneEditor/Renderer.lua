local GameSettings = require('std.GameSettings')
local EntityManager = require('std.EntityManager')
local Magic = require('std.Magic')

local EditorState = require('editor.EditorState')
local Grid = require('editor.Grid')
local Textures = require('editor.Textures')

local function renderSquare(r, g, b, x, y, width, height, lineWidth, alpha)
    Magic.render_set_alpha(alpha)
    Magic.render_set_depth(0.9)
    Magic.render_rect_colour(
        r,
        g,
        b,
        x - width / 2,
        y,
        lineWidth,
        height
    )
    Magic.render_rect_colour(
        r,
        g,
        b,
        x + width / 2,
        y,
        lineWidth,
        height
    )
    Magic.render_rect_colour(
        r,
        g,
        b,
        x,
        y - height / 2,
        width,
        lineWidth
    )
    Magic.render_rect_colour(
        r,
        g,
        b,
        x,
        y + height / 2,
        width,
        lineWidth
    )
    Magic.render_set_depth(0)
    Magic.render_set_alpha(1)
end

local function renderGrid(cameraZoom)
    local gridWidth = 2
    Magic.render_set_alpha(0.1 * cameraZoom)
    for i = 1,  GameSettings.GAME_WIDTH / Grid.GRID_SIZE  do
        Magic.render_rect_colour(
            1,
            1,
            1,
            i * Grid.GRID_SIZE,
            GameSettings.GAME_HALF_HEIGHT,
            gridWidth,
            GameSettings.GAME_HEIGHT
        )
    end

    for i = 1,  GameSettings.GAME_HEIGHT / Grid.GRID_SIZE  do
        Magic.render_rect_colour(
            1,
            1,
            1,
            GameSettings.GAME_HALF_WIDTH,
            i * Grid.GRID_SIZE,
            GameSettings.GAME_WIDTH,
            gridWidth
        )
    end
    Magic.render_set_alpha(1)
end

local function renderSelected(x, y, width, height)
    local size = 10
    width = width + size
    height = height + size

    renderSquare(0.2, 1, 1, x, y, width, height, 2, 0.5)
end

local function render()
    -- Draw a black background to make it visible in the editor
    -- where the game viewport is
    local ratio = EditorState.maxScroll.x / EditorState.maxScroll.y
    local _, _, cameraZoom = Magic.get_camera()

    Magic.render_set_tiling(40, 40 / ratio)
    Magic.render_rect(
        Textures.BACKGROUND,
        0,
        0,
        EditorState.maxScroll.x,
        EditorState.maxScroll.y
    )
    Magic.render_set_tiling(1, 1)

    EntityManager.removeDeletedEntities()
    local entityIterator = EntityManager.entityIterator()
    local entity = entityIterator()
    while entity ~= nil do
        entity:updatePhysics()
        if entity.texture ~= 0 then
            local alphaBefore = entity.alpha
            if EditorState.currentLayer ~= entity.layer then
                entity.alpha = 0.5
            end
            entity:render()
            entity.alpha = alphaBefore
        else
            Magic.render_text(
                entity.name,
                entity.x - string.len(entity.name) * 14,
                entity.y,
                12
            )
        end
        entity = entityIterator()
    end

    if EditorState.currentTile ~= nil then
        Magic.render_set_depth(0.1)
        Magic.render_set_tiling(EditorState.tileDrag.tileDimension.x, EditorState.tileDrag.tileDimension.y)
        Magic.render_rect(
            EditorState.currentTile.texture,
            EditorState.tileDrag.position.x,
            EditorState.tileDrag.position.y,
            EditorState.tileDrag.dimensions.width,
            EditorState.tileDrag.dimensions.height
        )
        Magic.render_set_tiling(1, 1)
        Magic.render_set_depth(0)
    end

    renderSquare(
        1,
        0,
        0,
        GameSettings.GAME_HALF_WIDTH,
        GameSettings.GAME_HALF_HEIGHT,
        GameSettings.GAME_WIDTH,
        GameSettings.GAME_HEIGHT,
        2,
        0.8
    )

    if Grid.GRID_SIZE > 4 and EditorState.editorGameObjectMode == 'TILE' then
        renderGrid(cameraZoom)
    end
end

return {
    render = render,
    renderSelected = renderSelected,
}
local SceneManager = require('std.SceneManager')
local Magic = require('std.Magic')
local EntityManager = require('std.EntityManager')

local Renderer = require('editor.sceneEditor.Renderer')
local EditorState = require('editor.EditorState')
local Grid = require('editor.Grid')
local Utilities = require('editor.Utilities')

local lastMouseScroll = Magic.get_mouse_scroll()

local function scrollMouse()
    local newMouseScroll = Magic.get_mouse_scroll()
    local mouseX, mouseY = Magic.get_mouse_position()
    if Utilities.isWithinCamera(mouseX, mouseY) and newMouseScroll ~= lastMouseScroll then
        local diff = newMouseScroll - lastMouseScroll
        local cameraX, cameraY, cameraZoom = Magic.get_camera()

        local newZoom = cameraZoom + (diff / 10)
        newZoom = math.max(0.2, newZoom)
        newZoom = math.min(1.8, newZoom)

        Magic_move_camera(cameraX, cameraY, newZoom)
    end

    lastMouseScroll = newMouseScroll
end

local function entityGameLoop()
    local mouseX, mouseY = Magic.get_mouse_position()

    if Magic.is_mouse_button_pressed(0) and Utilities.isWithinCamera(mouseX, mouseY) then

        local cameraX, cameraY, cameraZoom = Magic.get_camera()
        local gridX, gridY = Grid:coords(mouseX, mouseY)

        if EditorState.moveCameraCoords.mouseX == -1 and EditorState.moveCameraCoords.mouseY == -1 then
            EditorState.moveCameraCoords.mouseX = mouseX
            EditorState.moveCameraCoords.mouseY = mouseY
        end

        if EditorState.entityDragged == nil then
            local entitiesSelected = Magic.query_bounds(mouseX - 1, mouseY - 1, mouseX + 1, mouseY + 1)

            EditorState.entityDragged = -1
            if #entitiesSelected > 0 then
                local entityIterator = EntityManager.entityIterator()
                local entity = entityIterator()
                while entity ~= nil do
                    for i = 1, #entitiesSelected do
                        if entity.layer == EditorState.currentLayer and entity.physicsEngineId == entitiesSelected[i] then
                            EditorState.entityDragged = entitiesSelected[i]
                            break
                        end
                    end
                    entity = entityIterator()
                end
            end
        end

        if EditorState.entityDragged == -1 then
            local moveX = cameraX + (EditorState.moveCameraCoords.mouseX - mouseX)
            local moveY = cameraY + (EditorState.moveCameraCoords.mouseY - mouseY)

            moveX = math.min(moveX, EditorState.maxScroll.x / 2)
            moveX = math.max(moveX, -EditorState.maxScroll.x / 2)

            moveY = math.min(moveY, EditorState.maxScroll.y / 2)
            moveY = math.max(moveY, -EditorState.maxScroll.y / 2)

            Magic_move_camera(
                moveX,
                moveY,
                cameraZoom
            )
            EditorState.lastEntityDragged = nil
        else
            local sceneEntity, physics = Utilities.getEntityDragged()

            if sceneEntity.tileData == nil then
                gridX = gridX + (Grid.GRID_SIZE / 2)
                gridY = gridY + (Grid.GRID_SIZE / 2)
            end

            if math.abs(mouseX - EditorState.moveCameraCoords.mouseX) > 1 or
                math.abs(mouseY - EditorState.moveCameraCoords.mouseY) > 1 then
                Magic.set_physics_body_position(EditorState.entityDragged,
                    gridX,
                    gridY,
                    physics.angle
                )
                physics.x = gridX
                physics.y = gridY
            end

            EditorState.lastEntityDragged = {
                physics = physics,
                scene = sceneEntity,
            }
        end
    elseif EditorState.entityDragged ~= nil then
        if EditorState.entityDragged ~= -1 then
            Utilities.repositionEntity()
            SceneManager.reloadScene()
        end

        EditorState.moveCameraCoords.mouseX = -1
        EditorState.moveCameraCoords.mouseY = -1
        EditorState.entityDragged = nil
    end
end

local function tileGameLoop()
    if EditorState.currentTile ~= nil then
        if Magic.is_mouse_button_pressed(0) then
            local mouseX, mouseY = Magic.get_mouse_position()
            local gridX, gridY = Grid:coords(mouseX, mouseY)

            if EditorState.tileDrag.startPos.x == nil and EditorState.tileDrag.startPos.y == nil then
                EditorState.tileDrag.startPos = {
                    x = gridX,
                    y = gridY
                }
            end
            EditorState.tileDrag.endPos = {
                x = gridX,
                y = gridY
            }
            EditorState.tileDrag.dimensions = {
                width = EditorState.tileDrag.endPos.x - EditorState.tileDrag.startPos.x,
                height = EditorState.tileDrag.endPos.y - EditorState.tileDrag.startPos.y,
            }
            EditorState.tileDrag.position = {
                x = EditorState.tileDrag.startPos.x + (EditorState.tileDrag.dimensions.width / 2),
                y = EditorState.tileDrag.startPos.y + (EditorState.tileDrag.dimensions.height / 2),
            }

            EditorState.tileDrag.tileDimension = {
                x = EditorState.tileDrag.dimensions.width // Grid.GRID_SIZE,
                y = EditorState.tileDrag.dimensions.height // Grid.GRID_SIZE,
            }
        else
            if EditorState.tileDrag.startPos.x ~= nil and EditorState.tileDrag.startPos.y ~= nil and
                EditorState.tileDrag.dimensions.width ~= 0 and EditorState.tileDrag.dimensions.height ~= 0 then
                local currentScene = SceneManager.currentScene()
                currentScene._addEntity(currentScene, EditorState.currentLayer, {
                    name = EditorState.currentTile.name,
                    x = EditorState.tileDrag.position.x,
                    y = EditorState.tileDrag.position.y,
                    depth = 0.0,
                    alpha = 1.0,
                    tileData = {
                        width = math.abs(EditorState.tileDrag.dimensions.width),
                        height = math.abs(EditorState.tileDrag.dimensions.height),
                        tiling = {
                            x = EditorState.tileDrag.tileDimension.x,
                            y = EditorState.tileDrag.tileDimension.y,
                        }
                    }
                })
                SceneManager.reloadScene()
            end

            EditorState.tileDrag = {
                startPos = {
                    x = nil,
                    y = nil,
                },
                endPos = {
                    x = nil
                },
                dimensions = {
                    width = nil,
                    height = nil
                },
                position = {
                    x = nil,
                    y = nil,
                },
                tileDimension = {
                    x = nil,
                    y = nil,
                }
            }
        end
    end
end

local function render()
    if EditorState.editorGameObjectMode == "ENTITY" then
        entityGameLoop()
    elseif EditorState.editorGameObjectMode == "TILE" then
        tileGameLoop()
    end

    scrollMouse()
    Renderer.render()

    if EditorState.lastEntityDragged ~= nil then
        Renderer.renderSelected(
            EditorState.lastEntityDragged.physics.x,
            EditorState.lastEntityDragged.physics.y,
            EditorState.lastEntityDragged.physics.width,
            EditorState.lastEntityDragged.physics.height
        )
    end
end

return {
    render = render
}
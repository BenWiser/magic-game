local SceneManager = require('std.SceneManager')
local EntityManager = require('std.EntityManager')
local GameSettings = require('std.GameSettings')
local Magic = require('std.Magic')
local ComponentManager = require('std.ComponentManager')

local EditorState = require('editor.EditorState')
local Utilities = require('editor.Utilities')
local Grid = require('editor.Grid')
local Textures = require('editor.Textures')
local WrapMagic = require('editor.WrapMagic')
local profiler = require('profiler.src.profiler')

profiler.configuration({
    lW = 0
})

local function redirectProfilingLogs(log)
    print(log)
end

profiler.attachPrintFunction(redirectProfilingLogs, true)

local sceneNames = SceneManager.getSceneNames()

local newLayerName = ""

local function renderEntitySelectors(currentScene)
    local entityTemplateNames = EntityManager.getEntityTemplateNames()
    local buttonSize = 55
    local sameLineIndex = 0

    for i = 1, #entityTemplateNames do
        local entityTemplate = EntityManager.getTemplate(entityTemplateNames[i])

        if not entityTemplate._isTile and EditorState.editorGameObjectMode == 'ENTITY' then
            if math.fmod(sameLineIndex, 4) ~= 0 then
                Magic_editor_sameLine()
            end
            sameLineIndex = sameLineIndex + 1

            local animationManager = entityTemplate.animationManager
            local frameWidth = 1 / animationManager.columns
            local frameHeight = 1 / animationManager.rows

            Magic_editor_beginGroup()
                Magic_editor_registerImageButton(entityTemplateNames[i], entityTemplate.texture, buttonSize, 0, 0, frameWidth, frameHeight, function ()
                    local cameraX, cameraY = Magic.get_camera()
                    local gridX, gridY = Grid:coords(cameraX, cameraY)
                    currentScene._addEntity(currentScene, EditorState.currentLayer, {
                        name = entityTemplateNames[i],
                        x = gridX,
                        y = gridY,
                        angle = 0,
                        depth = 0.0,
                        alpha = 1.0,
                        components = {},
                    })
                    SceneManager.reloadScene()
                end)
                Magic_editor_text(entityTemplateNames[i])
            Magic_editor_endGroup()

        elseif entityTemplate._isTile and EditorState.editorGameObjectMode == 'TILE' then
            if math.fmod(sameLineIndex, 4) ~= 0 then
                Magic_editor_sameLine()
            end
            sameLineIndex = sameLineIndex + 1

            Magic_editor_beginGroup()
                Magic_editor_registerImageButton(entityTemplate.name, entityTemplate.texture, buttonSize, 0, 0, 1, 1, function ()
                    EditorState.currentTile = entityTemplate
                end)
                Magic_editor_text(entityTemplateNames[i])
            Magic_editor_endGroup()
        end
    end
end

local function render(mouseX, mouseY, currentScene)
    Magic_editor_beginMenu()
        Magic_editor_registerMenuButton("Save scene", function()
            print("Saving current scene")
            Utilities.save()
        end)

        if EditorState.isRunning then
            Magic_editor_registerMenuButton((not EditorState.isProfiling and "Start" or "End") .. " Profiling", function()
                EditorState.isProfiling = not EditorState.isProfiling

                if EditorState.isProfiling then
                    profiler.start()
                else
                    profiler.stop()
                    profiler.report(' ')
                end
            end)
        end
    Magic_editor_endMenu()

    if not EditorState.isRunning then
        Magic_editor_beginStatus()
            local status = "[" .. math.floor(mouseX) .. ", " .. math.floor(mouseY) .. "]"

            status = status .. " Entity Mode: " .. EditorState.editorGameObjectMode

            if EditorState.editorGameObjectMode == "TILE" and EditorState.currentTile ~= nil then
                status = status .. " | Tile Selected: " .. EditorState.currentTile.name
            end

            Magic_editor_text(status)
        Magic_editor_end()
    end

    if not EditorState.isProfiling then
        Magic_editor_beginCenter()
            if not EditorState.isRunning then
                Magic_editor_registerImageButton("Play", Textures.PLAY, 20, 0, 0, 1, 1, function ()
                    Magic_move_camera(
                        EditorState.moveCameraCoords.cameraX,
                        EditorState.moveCameraCoords.cameraY,
                        EditorState.moveCameraCoords.cameraZoom
                    )
                    EditorState.isRunning = true
                    WrapMagic.enable()
                end)
            else
                Magic_editor_registerImageButton("Pause", Textures.PAUSE, 20, 0, 0, 1, 1, function ()
                    local cameraX, cameraY, cameraZoom = Magic.get_camera()
                    EditorState.moveCameraCoords.cameraX = cameraX
                    EditorState.moveCameraCoords.cameraY = cameraY
                    EditorState.moveCameraCoords.cameraZoom = cameraZoom
                    EditorState.isRunning = false

                    SceneManager.reloadScene()
                    WrapMagic.disable()
                end)
            end
        Magic_editor_end()
    end

    Magic_editor_beginSidebar()
        Magic_editor_beginSidebarChild("Scenes")
        for i = 1, #sceneNames do
            Magic_editor_registerButton(sceneNames[i], function ()
                print("Changing scene: " .. sceneNames[i])
                EditorState.lastEntityDragged = nil
                SceneManager.changeScene(sceneNames[i])
            end)
        end
        Magic_editor_endSidebarChild()

        if not EditorState.isRunning then
            Magic_editor_beginSidebarChild("Scene")
                Magic_editor_beginTabBar("Scene Tabs")
                    Magic_editor_registerTabButton("Layers", function ()
                        for i = 1, #currentScene._layers do
                            local selectedBefore = EditorState.currentLayer == i
                            Magic_editor_inputCheckbox(currentScene._layers[i].name, selectedBefore, function (isSelected)
                                if not selectedBefore and isSelected then
                                    EditorState.currentLayer = i
                                    EditorState.lastEntityDragged = nil
                                end
                            end)

                            if #currentScene._layers > 1 then
                                Magic_editor_sameLine()

                                Magic_editor_registerImageButton("Delete layer " .. i, Textures.BIN, 16, 0, 0, 1, 1, function ()
                                    table.remove(currentScene._layers, i)
                                    EditorState.lastEntityDragged = nil
                                    SceneManager.reloadScene()
                                end)
                            end
                        end

                        Magic_edtior_inputString("", newLayerName, function (val)
                            if newLayerName ~= val then
                                newLayerName = val
                            end
                        end)
                        Magic_editor_sameLine()
                        Magic_editor_registerImageButton("New Layer", Textures.NEW, 16, 0, 0, 1, 1, function ()
                            if newLayerName ~= "" then
                                table.insert(currentScene._layers, {
                                    name = newLayerName,
                                    entities = {},
                                })
                                newLayerName = ""
                            end
                        end)
                    end)

                    Magic_editor_registerTabButton("Variables", function ()
                        Magic_editor_inputNumber("Width", currentScene.width, function (val)
                            if val ~= currentScene.width then
                                currentScene.width = val
                                GameSettings:setGameDimensions(currentScene.width, currentScene.height)
                            end
                        end)
    
                        Magic_editor_inputNumber("Height", currentScene.height, function (val)
                            if val ~= currentScene.height then
                                currentScene.height = val
                                GameSettings:setGameDimensions(currentScene.width, currentScene.height)
                            end
                        end)
    
                        Magic_editor_inputNumber("Grid", Grid.GRID_SIZE, function (val)
                            if val ~= Grid.GRID_SIZE then
                                Grid.GRID_SIZE = val
                            end
                        end)
                    end)
                Magic_editor_endTabBar()
            Magic_editor_endSidebarChild()

            Magic_editor_beginSidebarChild("Game Objects")
                Magic_editor_beginTabBar("GameObjectsTabs")
                    Magic_editor_registerTabButton("Entities", function ()
                        EditorState.editorGameObjectMode = 'ENTITY'
                        renderEntitySelectors(currentScene)
                    end)

                    Magic_editor_registerTabButton("Tiles", function ()
                        EditorState.editorGameObjectMode = 'TILE'
                        EditorState.lastEntityDragged = nil
                        renderEntitySelectors(currentScene)
                    end)
                Magic_editor_endTabBar()
            Magic_editor_endSidebarChild()

            if EditorState.lastEntityDragged ~= nil then
                Magic_editor_beginSidebarChild("Entity")
                    Magic_editor_registerButton("Delete", function ()
                        local currentScene = SceneManager.currentScene()
                        currentScene._removeEntity(
                            currentScene,
                            EditorState.currentLayer,
                            EditorState.lastEntityDragged.scene
                        )
                        EditorState.lastEntityDragged = nil
                        SceneManager.reloadScene()
                    end)

                    Magic_editor_sameLine()

                    Magic_editor_beginTabBar("EntityTabs")
                        Magic_editor_registerTabButton("Variables", function ()
                            Magic_editor_inputNumber("x", EditorState.lastEntityDragged.physics.x, function (val)
                                if not Utilities.isWithinCamera(mouseX, mouseY) and val ~= EditorState.lastEntityDragged.physics.x then
                                    EditorState.lastEntityDragged.physics.x = val
                                    EditorState.lastEntityDragged.scene.x = val
                                    SceneManager.reloadScene()
                                end
                            end)

                            Magic_editor_inputNumber("y", EditorState.lastEntityDragged.physics.y, function (val)
                                if not Utilities.isWithinCamera(mouseX, mouseY) and val ~= EditorState.lastEntityDragged.physics.y then
                                    EditorState.lastEntityDragged.physics.y = val
                                    EditorState.lastEntityDragged.scene.y = val
                                    SceneManager.reloadScene()
                                end
                            end)

                            Magic_editor_inputNumber("angle", EditorState.lastEntityDragged.physics.angle, function (val)
                                if not Utilities.isWithinCamera(mouseX, mouseY) and val ~= EditorState.lastEntityDragged.physics.angle then
                                    EditorState.lastEntityDragged.physics.angle = val
                                    EditorState.lastEntityDragged.scene.angle = val
                                    SceneManager.reloadScene()
                                end
                            end)
                        end)

                        Magic_editor_registerTabButton("Components", function ()
                            local componentNames = ComponentManager.getNames()
                            for i = 1, #componentNames do
                                local name = componentNames[i]
                                local before = EditorState.lastEntityDragged.scene.components[name]
                                Magic_editor_inputCheckbox(name, before, function(val)
                                    if not Utilities.isWithinCamera(mouseX, mouseY) and before ~= val then
                                        EditorState.lastEntityDragged.scene.components[name] = val
                                        SceneManager.reloadScene()
                                    end
                                end)
                            end
                        end)
                    Magic_editor_endTabBar()
                Magic_editor_endSidebarChild()
            end
        else
            Magic_editor_beginSidebarChild("Scene Variables")
            for key, value in pairs(currentScene) do
                -- Only numbers can be modified currently
                if key ~= "sceneId" and key ~= "width" and key ~= "height" and type(value) == "number" then
                    Magic_editor_inputNumber(key, value, function (val)
                        if value ~= val then
                            currentScene[key] = val
                        end
                    end)
                end
            end
            Magic_editor_endSidebarChild()
        end
    Magic_editor_endSidebar()
end

return {
    render = render
}
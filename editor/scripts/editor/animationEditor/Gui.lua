local EntityManager = require('std.EntityManager')

local EditorState = require('editor.EditorState')
local Textures = require('editor.Textures')

local newAnimationName = ""

local function render()
    Magic_editor_beginMenu()
    Magic_editor_registerMenuButton("Save animations", function()
        print("Saving animations")
        EntityManager.saveTemplates()
    end)
    Magic_editor_endMenu()

    Magic_editor_beginSidebar()
        Magic_editor_beginSidebarChild("Entities")
            local entityTemplateNames = EntityManager.getEntityTemplateNames()
            for i = 1, #entityTemplateNames do
                local entityTemplate = EntityManager.getTemplate(entityTemplateNames[i])
                if not entityTemplate._isTile then
                    Magic_editor_registerButton(entityTemplateNames[i], function ()
                        EditorState.AnimationEditor.entity = entityTemplateNames[i]
                        EditorState.AnimationEditor.selectedAnimation = nil
                        newAnimationName = ""
                    end)
                end
            end
        Magic_editor_endSidebarChild()

        if EditorState.AnimationEditor.entity ~= nil then
            local entity = EntityManager.getTemplate(EditorState.AnimationEditor.entity)
            Magic_editor_beginSidebarChild("Settings")
                Magic_editor_inputNumber("Rows", entity.animationManager.rows, function (val)
                    if entity.animationManager.rows ~= val then
                        entity.animationManager.rows = val
                    end
                end)
                Magic_editor_inputNumber("Columns", entity.animationManager.columns, function (val)
                    if entity.animationManager.columns ~= val then
                        entity.animationManager.columns = val
                    end
                end)
            Magic_editor_endSidebarChild()

            Magic_editor_beginSidebarChild("Animations")
            for animationName in pairs(entity.animationManager.animations) do
                Magic_editor_registerButton(animationName, function()
                    EditorState.AnimationEditor.selectedAnimation = animationName
                end)

                Magic_editor_sameLine()

                Magic_editor_registerButton("Delete " .. animationName .. " animation", function()
                    EditorState.AnimationEditor.animationToDelete = animationName
                end)
            end
            Magic_edtior_inputString("", newAnimationName, function (val)
                if newAnimationName ~= val then
                    newAnimationName = val
                end
            end)
            Magic_editor_sameLine()
            Magic_editor_registerImageButton("Create animation", Textures.NEW, 16, 0, 0, 1, 1, function ()
                if newAnimationName ~= "" then
                    entity.animationManager.animations[newAnimationName] = {}
                    newAnimationName = ""
                end
            end)
            Magic_editor_endSidebarChild()

            if EditorState.AnimationEditor.animationToDelete ~= nil then
                entity.animationManager.animations[EditorState.AnimationEditor.animationToDelete] = nil
                EditorState.AnimationEditor.selectedAnimation = nil
                EditorState.AnimationEditor.animationToDelete = nil
            end

            if EditorState.AnimationEditor.selectedAnimation ~= nil then
                Magic_editor_beginSidebarChild("Frames")
                local frameWidth = 1 / entity.animationManager.columns
                local frameHeight = 1 / entity.animationManager.rows
                local sameLineIndex = 0

                for row = 0, entity.animationManager.rows do
                    for column = 0, entity.animationManager.columns do
                        if math.fmod(sameLineIndex, 3) ~= 0 then
                            Magic_editor_sameLine()
                        end
                        sameLineIndex = sameLineIndex + 1

                        Magic_editor_registerImageButton(
                            EditorState.AnimationEditor.selectedAnimation .. row .. " " .. column,
                            entity.texture,
                            65,
                            frameWidth * row, frameHeight * column,
                            frameWidth * row + frameWidth, frameHeight * column + frameHeight,
                        function ()
                            table.insert(
                                entity.animationManager.animations[EditorState.AnimationEditor.selectedAnimation],
                                {row, column}
                            )
                        end)
                    end
                end
                Magic_editor_endSidebarChild()
            end
        end
    Magic_editor_endSidebar()
end

return {
    render = render
}
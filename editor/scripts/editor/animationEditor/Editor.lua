local Magic = require('std.Magic')
local GameSettings = require('std.GameSettings')
local EntityManager = require('std.EntityManager')

local EditorState = require('editor.EditorState')
local Textures = require('editor.Textures')

local frame = 1
local animationSpeed = 0.05
local mouseClicked = false
local frameToRemove = nil

local function render()
    local cameraX, cameraY = Magic.get_camera()
    local backgroundRatio = EditorState.maxScroll.x / EditorState.maxScroll.y
    Magic_move_camera(cameraX, cameraY, 1)


    Magic.render_set_tiling(40, 40 / backgroundRatio)
    Magic.render_rect(
        Textures.BACKGROUND,
        0,
        0,
        EditorState.maxScroll.x,
        EditorState.maxScroll.y
    )
    Magic.render_set_tiling(1, 1)

    if EditorState.AnimationEditor.selectedAnimation ~= nil then
        local entitySelected = EntityManager.getTemplate(EditorState.AnimationEditor.entity)
        local selectedAnimation = EditorState.AnimationEditor.selectedAnimation
        local frames = entitySelected.animationManager.animations[selectedAnimation]

        if #frames > 0 then
            frame = frame + animationSpeed
    
            if frame > #frames + 1 then
                frame = 1
            end
    
            -- The animation looping
    
            Magic.render_spritemap_frame(
                entitySelected.texture,
                entitySelected.animationManager.columns,
                entitySelected.animationManager.rows,
                frames[math.floor(frame)][1],
                frames[math.floor(frame)][2],
                cameraX,
                cameraY,
                200,
                200,
                0
            )
    
            -- All of the frames of the animation
    
            local previewsSize = 100
    
            for i = 1, #frames do
                local x = cameraX + i * previewsSize - GameSettings.GAME_HALF_WIDTH
                local y = cameraY + GameSettings.GAME_HALF_HEIGHT
                if Magic.is_mouse_button_pressed(0) then
                    local mouseX, mouseY = Magic.get_mouse_position()
    
                    if not mouseClicked and math.abs(mouseX - x) < previewsSize / 2 and math.abs(mouseY - y) < previewsSize / 2 then
                        mouseClicked = true
                        frameToRemove = i
                    end
                else
                    mouseClicked = false
                end
    
                if math.floor(frame) == i then
                    Magic.render_set_alpha(0.5)
                    Magic.render_rect_colour(
                        0.01,
                        0.48,
                        0.74,
                        x,
                        y,
                        previewsSize,
                        previewsSize
                    )
                    Magic.render_set_alpha(1)
                end
    
                Magic.render_spritemap_frame(
                    entitySelected.texture,
                    entitySelected.animationManager.columns,
                    entitySelected.animationManager.rows,
                    frames[i][1],
                    frames[i][2],
                    x,
                    y,
                    previewsSize,
                    previewsSize,
                    0
                )
            end
    
            if frameToRemove ~= nil then
                table.remove(frames, frameToRemove)
                frameToRemove = nil
            end
        end
    end
end

return {
    render = render,
}
local Magic = require('std.Magic')

local isRunning = true
local moveCameraCoords = {
    mouseX = -1,
    mouseY = -1,
    cameraX = -1,
    cameraY = -1,
    cameraZoom = -1,
}
local entityDragged = nil
local lastEntityDragged = nil
local editorMode = 'SCENE'
local editorGameObjectMode = 'ENTITY'
local currentTile = nil
local tileDrag = {
    startPos = {
        x = nil,
        y = nil,
    },
    endPos = {
        x = nil
    },
    dimensions = {
        width = nil,
        height = nil
    },
    position = {
        x = nil,
        y = nil,
    },
    tileDimension = {
        x = nil,
        y = nil,
    }
}

local screenX, screenY = Magic.get_screen_dimensions()

local maxScroll = {
    x = screenX * 10,
    y = screenY * 10
}

local AnimationEditor = {
    entity = nil,
    selectedAnimation = nil,
    animationToDelete = nil,
}

local isProfiling = false

local currentLayer = 1

return {
    isRunning = isRunning,
    moveCameraCoords = moveCameraCoords,
    entityDragged = entityDragged,
    editorGameObjectMode = editorGameObjectMode,
    editorMode = editorMode,
    currentTile = currentTile,
    tileDrag = tileDrag,
    lastEntityDragged = lastEntityDragged,
    AnimationEditor = AnimationEditor,
    maxScroll = maxScroll,
    isProfiling = isProfiling,
    currentLayer = currentLayer,
}
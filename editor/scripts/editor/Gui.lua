local SceneManager = require('std.SceneManager')
local Magic = require('std.Magic')

local EditorState = require('editor.EditorState')
local SceneEditorGui = require('editor.sceneEditor.Gui')
local AnimationEditorGui = require('editor.animationEditor.Gui')


local function render()
    local mouseX, mouseY = Magic.get_mouse_position()
    local currentScene = SceneManager.currentScene()

    Magic_editor_beginViewportTopSection()
        Magic_editor_beginTabBar("Top Section")
            Magic_editor_registerTabButton("Scene", function ()
                EditorState.editorMode = 'SCENE'
            end)

            if not EditorState.isProfiling then
                Magic_editor_registerTabButton("Animation Editor", function ()
                    EditorState.editorMode = 'ANIMATION_EDITOR'
                end)
            end
        Magic_editor_endTabBar()
    Magic_editor_end()

    if EditorState.editorMode == 'SCENE' then
        SceneEditorGui.render(mouseX, mouseY, currentScene)
    elseif EditorState.editorMode == 'ANIMATION_EDITOR' then
        AnimationEditorGui.render()
    end
end

return {
    render = render
}
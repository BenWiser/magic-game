-- Wrap the magic APIs that can interfere with the editor
-- These APIs will need to be called with the global native methods directly

local Magic = require('std.Magic')

local move_camera = Magic.move_camera

local function disable()
    Magic.move_camera = function ()
    end
end

local function enable()
    Magic.move_camera = move_camera
end

return {
    disable = disable,
    enable = enable,
}
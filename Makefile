.PHONY: test

BINARY_NAME=game

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Darwin)
	PLATFORM=macosx
	CC_COMPILER=/usr/bin/clang
	CXX_COMPILER=/usr/bin/clang++
endif
ifeq ($(UNAME_S), Linux)
	PLATFORM=linux
	CC_COMPILER=$(where gcc-8)
	CXX_COMPILER=$(where g++-8)
endif

export CC=$(CC_COMPILER)
export CXX=$(CXX_COMPILER)

BUILD_DIR=./build
INSTALL_DIR=./install
SOURCE_DIR=./src
LIB_DIR=./lib

build: bundle install
	make -C $(INSTALL_DIR)/engine
	make -C $(INSTALL_DIR)/cli

bundle: install
	npm run bundle
	$(LIB_DIR)/lua/src/luac -o assets/lib std/dist/bundled.lua

release/macosx: build
	mkdir -p $(BINARY_NAME).app/Contents/MacOS
	mkdir -p $(BINARY_NAME).app/Contents/Resources
	cp $(INSTALL_DIR)/engine/$(BINARY_NAME) $(BINARY_NAME).app/Contents/MacOS
	cp -R assets $(BINARY_NAME).app/Contents/Resources

run: build
	$(INSTALL_DIR)/engine/$(BINARY_NAME)

editor: build
	$(INSTALL_DIR)/engine/$(BINARY_NAME)_editor ./editor/scripts/editor.lua ./editor

levelEditor: build
	$(INSTALL_DIR)/engine/$(BINARY_NAME) levelEditor.lua

test: install
	make -C $(INSTALL_DIR)/test
	$(INSTALL_DIR)/test/test --gtest_output="xml:report.xml"

install:
	npm install
	cp -f hooks/pre-commit .git/hooks
	git submodule update --init
	mkdir -p $(INSTALL_DIR)/Box2D
	(cd $(LIB_DIR)/lua && make $(PLATFORM))
	(cd $(INSTALL_DIR)/Box2D && \
		g++ -w -std=c++14 \
			-I../../$(LIB_DIR)/Box2D ../../$(LIB_DIR)/Box2D/Box2D/**/*.cpp \
			../../$(LIB_DIR)/Box2D/Box2D/**/**/*.cpp \
			-c)
	ar -crs $(INSTALL_DIR)/Box2D/libbox2d.a $(INSTALL_DIR)/Box2D/*.o
	(mkdir -p $(INSTALL_DIR)/glfw && cd $(INSTALL_DIR)/glfw && cmake ../.$(LIB_DIR)/glfw)
	(cd $(INSTALL_DIR)/glfw && make -j4)
	(mkdir -p $(INSTALL_DIR)/googletest)
	(cd $(INSTALL_DIR)/googletest && export CC=${CC_COMPILER} && export CXX=${CC_COMPILER} && cmake ../.$(LIB_DIR)/googletest)
	(cd $(INSTALL_DIR)/googletest && make -j4)
	mkdir -p $(INSTALL_DIR)/engine
	(cd $(INSTALL_DIR)/engine && cmake ../../engine)
	mkdir -p $(INSTALL_DIR)/test
	(cd $(INSTALL_DIR)/test && cmake ../../test)
	mkdir -p $(INSTALL_DIR)/openal-soft
	(cd $(INSTALL_DIR)/openal-soft && cmake ../../lib/openal-soft && make -j4)
	mkdir -p $(INSTALL_DIR)/freetype2
	(cd $(INSTALL_DIR)/freetype2 && cmake -D BUILD_SHARED_LIBS=true -D CMAKE_BUILD_TYPE=Release ../../lib/freetype2 && make -j4)
	mkdir -p $(INSTALL_DIR)/cli
	(cd $(INSTALL_DIR)/cli && cmake ../../cli)

local Camera = require('std.Camera')
local Component = require('std.Component')

Component:new('Camera Follow', {
    initialise = function (entity)
        entity.camera_zoom = 1.4
        Camera.followEntity(entity, entity.camera_zoom, false)
    end,
    update = function (entity)
        Camera.followEntity(entity, entity.camera_zoom, true)
    end,
})
local Component = require('std.Component')
local Magic = require('std.Magic')
local Physics = require('std.Physics')
local Input = require('std.Input')

Component:new('Keyboard Control', {
    initialise = function (entity)
        entity.jump_speed = 4000
    end,
    update = function (entity)
        local speed = 0
        if Input.isPressed('right') then
            entity.flippedHorizantal = false
            speed = entity.scene.move_speed
        elseif Input.isPressed('left') then
            entity.flippedHorizantal = true
            speed = -entity.scene.move_speed
        end

        Physics.moveHorizontal(entity, speed)

        local _, forceY = Magic.get_physics_body_velocity(entity.physicsEngineId)

        if Input.isPressed('up') and entity:touchingComponent('Solid') ~= nil then
            if forceY == 0 then
                Magic.apply_force_to_physics_body(entity.physicsEngineId, 0, -entity.jump_speed)
            end
        end

        if forceY > 0 then
            Magic.apply_force_to_physics_body(entity.physicsEngineId, 0, 200)
        end
    end
})
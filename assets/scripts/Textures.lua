local Texture = require('std.Texture')

return {
    Background = Texture:new('./assets/textures/background.png'),
    CastleWall = Texture:new('./assets/textures/castleWall.png'),
    Box = Texture:new('./assets/textures/box.png'),
    Wall = Texture:new('./assets/textures/wall.png'),
    Floor = Texture:new('./assets/textures/floor.png'),
    PLAYER = Texture:new('./assets/textures/player.png')
}
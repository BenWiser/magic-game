local Entity = require('std.Entity')

local Textures = require('Textures')

Entity:new('Box', {
    texture = Textures.Box,
    width = 32,
    height = 32,
})
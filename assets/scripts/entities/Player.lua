local Entity = require('std.Entity')
local Input = require('std.Input')

local Textures = require('Textures')

Entity:new('Player', {
    texture = Textures.PLAYER,
    width = 20,
    height = 64,
    textureWidth = 64,
    friction = 0,
    fixedRotation = true,
    update = function (entity)
        if Input.isPressed('action') then
            entity.animationManager:startAnimation('Attack', 0.15)
        elseif Input.isPressed('left') then
            entity.animationManager:startAnimation('Run', 0.15)
        elseif Input.isPressed('right') then
            entity.animationManager:startAnimation('Run', 0.15)
        else
            entity.animationManager:startAnimation('Idle', 0.05)
        end
    end
})
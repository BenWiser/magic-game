-- Set any game settings you'd like to use
local GameSettings = require('std.GameSettings')

GameSettings:enablePhysics()

GameSettings:setGravity(0, 10)

-- Add the components that should be available to entities
require 'components.CameraFollow'
require 'components.KeyboardControl'
require 'components.Solid'

-- Add the tiles used in your levels
require 'tiles.Floor'
require 'tiles.Wall'

-- Add entities you'd like the game to have
require 'entities.Box'
require 'entities.Player'

-- Add your game scenes in the order you want them to be loaded
require 'scenes.Example'

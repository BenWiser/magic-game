local Tile = require('std.Tile')
local Textures = require('Textures')

Tile:new('Floor', {
    texture = Textures.Floor,
    initialise = function (tile)
        tile:addComponent('Solid')
    end
})
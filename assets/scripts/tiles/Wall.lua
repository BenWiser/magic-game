local Tile = require('std.Tile')
local Textures = require('Textures')

Tile:new('Wall', {
    texture = Textures.Wall,
    initialise = function (tile)
        tile:addComponent('Solid')
    end
})
local Scene = require('std.Scene')
local Background = require('std.Background')
local Magic = require('std.Magic')

local Textures = require('Textures')

Scene:new('Example', {
    initialise = function(scene)
        -- The speed of the player
        scene.move_speed = 25
    end,
    update = function (scene)
        Background.renderParallax(Textures.Background)

        Magic.render_set_tiling(2, 1.8)
        Background.renderParallax(Textures.CastleWall, 1.1)
        Magic.render_set_tiling(1, 1)
    end
})

#include <string>

#include "gtest/gtest.h"

#include "FileReader.h"

TEST(FileReader, readFile)
{
    auto expectedResult = "THIS FILE HAS SOME STUFF\n"
                          "AND GOES TO THE NEXT LINE";

    auto actualResult = FileReader::readFile("./test/test-files/TEST_FILE.txt");
    EXPECT_EQ(expectedResult, actualResult);
}

TEST(FileReader, noFile)
{
    auto expectedResult = "";

    auto actualResult = FileReader::readFile("./FILE_DOES_NOT_EXIST");

    EXPECT_EQ(expectedResult, actualResult);
}

#include "gtest/gtest.h"

#include "renderer/RenderPipeline.h"

TEST(RenderPipeline, getPipeLine_IsSingleTon)
{
    auto pipelineFirst = RenderPipeline::getPipeLine();
    auto pipelineSecond = RenderPipeline::getPipeLine();

    // Should be the same reference - ie: singleton
    ASSERT_EQ(pipelineFirst.get(), pipelineSecond.get());
}

TEST(RenderPipeline, hasNext)
{
    auto pipeline = RenderPipeline::getPipeLine();

    ASSERT_FALSE(pipeline->hasNext());

    RenderPipeline::RenderCommand testCommand{
        "test"};

    pipeline->enque(testCommand);

    ASSERT_TRUE(pipeline->hasNext());

    // CLEAR FOR OTHER TESTS
    pipeline->dequeue();
}

TEST(RenderPipeline, correctQueueOrder)
{
    auto pipeline = RenderPipeline::getPipeLine();

    RenderPipeline::RenderCommand firstCommand{
        "first"};

    RenderPipeline::RenderCommand secondCommand{
        "second"};

    pipeline->enque(firstCommand);
    pipeline->enque(secondCommand);

    auto firstResult = pipeline->dequeue();
    auto secondResult = pipeline->dequeue();

    ASSERT_EQ(firstCommand.type, firstResult.type);
    ASSERT_EQ(secondCommand.type, secondResult.type);
}

TEST(RenderPipeline, createRectColour)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->createRectColour(
        0,
        0,
        0,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f);

    ASSERT_EQ(command.type, "rect_colour");
}

TEST(RenderPipeline, createRect)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->createRect(
        0,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f);

    ASSERT_EQ(command.type, "rect");
}

TEST(RenderPipeline, createSpriteMap)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->createSpriteMap(
        0,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f);

    ASSERT_EQ(command.type, "spritemap");
}

TEST(RenderPipeline, createText)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->createText(
        "hey",
        "fontPath",
        0.0f,
        0.0f,
        0.0f);

    ASSERT_EQ(command.type, "text");
}

TEST(RenderPipeline, setAlpa)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->setAlpha(0.5f);

    ASSERT_EQ(command.type, "alpha");
}

TEST(RenderPipeline, setDepth)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->setDepth(0.5f);

    ASSERT_EQ(command.type, "depth");
}

TEST(RenderPipeline, setTiling)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->setTiling(1.0f, 1.0f);

    ASSERT_EQ(command.type, "tiling");
}

TEST(RenderPipeline, setShader)
{
    auto pipeline = RenderPipeline::getPipeLine();

    auto command = pipeline->setShader(1);

    ASSERT_EQ(command.type, "shader");
}
cmake_minimum_required(VERSION 3.15)
project(test)

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(Threads REQUIRED)

set(LIB_DIR ../lib)
set(INSTALL_DIR ../install)

set(SOURCE_DIR ../engine/src)
set(TEST_SOURCE_DIR ./src)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

link_directories(
    ${INSTALL_DIR}/glfw/src
    ${INSTALL_DIR}/Box2D
    ${LIB_DIR}/lua/src
    ${INSTALL_DIR}/freetype2
    ${INSTALL_DIR}/googletest/lib)

set(INCLUDE_DIRS
    ../engine/include
    ${LIB_DIR}/glfw/include
    ${LIB_DIR}/lodepng/include
    ${LIB_DIR}/lua/src
    ${LIB_DIR}/Box2D
    ${LIB_DIR}/freetype2/include
    ${LIB_DIR}/googletest/googletest/include)

set(LIBS
    glfw3
    box2d
    lua
    freetype
    gtest
    gtest_main)

if (UNIX AND NOT APPLE)
    list(APPEND LIBS
        stdc++fs
        X11
        GLEW
        ${OPENGL_LIBRARIES}
        ${GLEW_LIBRARY}
        ${CMAKE_DL_LIBS})
    
    list(APPEND INCLUDE_DIRS
        ${OPENGL_INCLUDE_DIRS}
        ${GLEW_INCLUDE_DIRS})
endif()

if (APPLE)
    list(APPEND LIBS
        c++
        "-framework Cocoa"
        "-framework OpenGL"
        "-framework IOKit"
        "-framework CoreVideo")
endif()

include_directories(${INCLUDE_DIRS})

set(SOURCE_FILES
    ${TEST_SOURCE_DIR}/FileReader.cpp
    ${TEST_SOURCE_DIR}/RenderPipeline.cpp
    ${SOURCE_DIR}/Configuration.cpp
    ${SOURCE_DIR}/GameState.cpp
    ${SOURCE_DIR}/FileReader.cpp
    ${SOURCE_DIR}/renderer/GLRenderer.cpp
    ${SOURCE_DIR}/renderer/VBO.cpp
    ${SOURCE_DIR}/renderer/shaders/Shader.cpp
    ${SOURCE_DIR}/renderer/shaders/ShaderFactory.cpp
    ${SOURCE_DIR}/renderer/shaders/ShaderManager.cpp
    ${SOURCE_DIR}/font/Font.cpp
    ${SOURCE_DIR}/font/FontManager.cpp
    ${SOURCE_DIR}/renderer/RenderPipeline.cpp
    ${SOURCE_DIR}/editor/production/Editor.cpp)

add_executable(test ${SOURCE_FILES})

target_link_libraries(test ${LIBS} ${CMAKE_THREAD_LIBS_INIT})
#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include <string>

class Configuration
{
    public:
        Configuration();
    
    std::string getGameName();

    private:
        std::string _game_name;
};

#endif // _CONFIGURATION_H_
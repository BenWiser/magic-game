#ifndef _SCRIPT_ENGINE_H
#define _SCRIPT_ENGINE_H

#include <string>
#include <functional>

#include "lua.hpp"

class ScriptEngine
{
private:
    lua_State *_state;

public:
    void init();
    ~ScriptEngine();
    int loadBytecode(const std::string &fileName) const;
    int loadScript(const std::string &script) const;
    int addAPI(const std::string &name, int (*scriptFunction)(lua_State* state)) const;
    int loadLibrary(const std::string &name, const std::string &script) const;
    int execute();
    void gameLoop();
    void cleanUp();
};

#endif
#ifndef _SCRIPT_LOADER_H
#define _SCRIPT_LOADER_H

#include <string>

#include "scripting/ScriptEngine.h"

namespace ScriptLoader
{
using std::string;

void loadScript(
    const ScriptEngine &scriptEngine,
    const string &mainFileName);
void loadLibraries(const ScriptEngine &scriptEngine, const string &directory);
}; // namespace ScriptLoader

#endif
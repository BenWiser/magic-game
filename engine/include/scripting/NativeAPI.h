#ifndef _NATIVE_API_H
#define _NATIVE_API_H

#include "lua.hpp"

/**
 * Create a new texture and receive a texture id
 * @param {String} fileName
 * @return {Integer} textureId
 */
int create_texture(lua_State *state);

/**
 * Destroy the texture
 * @param {Integer} textureId
 * @return {void}
 */
int destroy_texture(lua_State *state);

/**
 * Render a rectangle with a colour
 * @param {Number} red
 * @param {Number} green
 * @param {Number} blue
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @return {void}
 */
int render_rect_colour(lua_State *state);

/**
 * Render a rectangle
 * @param {Integer} textureId
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @return {void}
 */
int render_rect(lua_State *state);

/**
 * Render a frame of an animation from a sprite map
 * @param {Integer} textureId
 * @param {Number} maxFrames
 * @param {Number} animations
 * @param {Number} frame
 * @param {Number} animation
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @return {void}
 */
int render_spritemap_frame(lua_State *state);

/**
 * Render some text
 * @param {String} text
 * @param {String} fontPath
 * @param {number} x
 * @param {number} y
 * @param {number} fontSize
 */
int render_text(lua_State *state);

/**
 * Set the renderers alpha value
 * @param {Number} alpha
 * @return {void}
 */
int render_set_alpha(lua_State *state);

/**
 * Set the renderers depth value
 * @param {Number} depth A value between -1 and 1, defaults to 0
 * @return {void}
 */
int render_set_depth(lua_State *state);

/**
 * Set the renderers tiling
 * @param {Number} horizontalTiling
 * @param {Number} verticalTiling
 */
int render_set_tiling(lua_State *state);

/**
 * Create a new shader program
 * @param {String} vertexShaderSource
 * @param {String} fragmentShaderSource
 * @return {Integer} shaderId
 */
int render_shader_create(lua_State *state);

/**
 * Set the current shader to be used by the renderer
 * @param {Integer} shaderId
 * @return {void}
 */
int render_shader_set(lua_State* state);

/**
 * Set the current shader to be used by the renderer to be the default shader
 * @return {void}
 */
int render_shader_set_default(lua_State* state);

/**
 * Iterate the physics engine
 * @return {void}
 */
int iterate_physics(lua_State* state);

/**
 * Set the gravity direction of the physics engine
 * @param {Number} x
 * @param {Number} y
 * @return {void}
 */
int set_physics_gravity(lua_State* state);

/**
 * Create physics body
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @param {Number} friction
 * @param {Number} density
 * @param {Boolean} isDynamic
 * @param {Boolean} fixedRotation
 * @param {Boolean} isSensor
 * @return {Integer} bodyId
 */
int create_physics_body(lua_State *state);

/**
 * Apply a force to a physics body
 * @param {Integer} bodyId
 * @param {Number} forceX
 * @param {Number} forceY
 * @return {void}
 */
int apply_force_to_physics_body(lua_State *state);

/**
 * Apply an impusle to a physics body
 * @param {Integer} bodyId
 * @param {Number} forceX
 * @param {Number} forceY
 * @return {void}
 */
int apply_impulse_to_physics_body(lua_State *state);

/**
 * Apply an angular force to a physics body
 * @param {Integer} bodyId
 * @param {Number} angle
 * @return {void}
 */
int apply_angular_impulse_to_physics_body(lua_State *state);

/**
 * Destroy a physics body
 * @param {Number} bodyId
 * @return {void}
 */
int destroy_physics_body(lua_State *state);

/**
 * Get the x and y postion, and rotation of the physics body
 * @param {Integer} bodyId
 * @return {Number, Number, Number} x, y, rotation
 */
int get_physics_body_position(lua_State *state);

/**
 * Set the x and y postion, and rotation of the physics body
 * @param {Integer} bodyId
 * @param {Number} x
 * @param {Number} y
 * @param {Number} rotation
 * @return {void}
 */
int set_physics_body_position(lua_State* state);

/**
 * Set the linear velocity for the physics body
 * @param {Integer} bodyId
 * @param {Number} forceX
 * @param {Number} forceY
 * @return {void}
 */
int set_physics_body_velocity(lua_State *state);

/**
 * Get the linear velocity for the physics body
 * @param {Integer} bodyId
 * @return {Number, Number} x, y
 */
int get_physics_body_velocity(lua_State *state);

/**
 * Return if two physics bodies have collided
 * @param {Integer} bodyIdA
 * @param {Integer} bodyIdB
 */
int physics_has_collided(lua_State *state);

/**
 * Return the physics body a body has collided with
 * @param {Integer} bodyId
 */
int physics_query_collisions(lua_State *state);

/**
 * Return if a key code has been pressed
 * @param {Integer} keyCode
 * @return {boolean} isPressed
 */
int is_key_pressed(lua_State *state);

/**
 * Return if a mouse button has been pressed
 * @param {Integer} mouseButton
 * @return {boolean} isPressed
 */
int is_mouse_button_pressed(lua_State *state);

/**
 * Return the mouse x and y
 * @return {Number, Number} x, y
 */
int get_mouse_position(lua_State *state);

/**
 * Returns the scroll of the mouse since the game started
 * @return {Number} scroll
 */
int get_mouse_scroll(lua_State *state);

/**
 * Perform a ray trace between two points and return the bodies collided with
 *
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @return {Table<Integer>} bodyids
 */
int ray_trace(lua_State *state);

/**
 * Query bounds for physics bodies
 *
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @return {Table<Integer>} bodyids
 */
int query_bounds(lua_State *state);

/**
 * Get screen dimenions
 * @return {Number, Number} width, height
 */
int get_screen_dimensions(lua_State *state);

/**
 * Move the camera and change the zoom
 * @param {Number} x
 * @param {Number} y
 * @param {Number} zoom
 * @return {void}
 */
int move_camera(lua_State *state);

/**
 * Get the camera position and zoom
 * @return {Number, Number, Number} x, y, zoom
 */
int get_camera(lua_State *state);

/**
 * Write text to a file
 * @param {String} fileName
 * @param {String} data
 * @return {void}
 */
int file_write(lua_State *state);

/**
 * Read from a file
 * @param {String} fileName
 * @return {String} output
 */
int file_read(lua_State *state);

/**
 * Load a sound file
 * Must be a wav file
 * @param {String} fileName
 * @return {Integer} soundId
 */
int sound_load(lua_State *state);

/**
 * Play a sound id
 * @param {Integer} soundId
 * @return {void}
 */
int sound_play(lua_State *state);

/**
 * Used to redirect print statements
 */
int print(lua_State* state);

#endif
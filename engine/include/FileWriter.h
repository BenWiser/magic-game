#ifndef _FILE_WRITER_H
#define _FILE_WRITER_H

#include <string>

namespace FileWriter
{
using std::string;

void writeFile(string fileName, string output);
} // namespace FileWriter

#endif
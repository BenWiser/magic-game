
#ifndef _FILE_READER_H
#define _FILE_READER_H

#include <string>

namespace FileReader
{
using std::string;

string readFile(string fileName);
} // namespace FileReader

#endif
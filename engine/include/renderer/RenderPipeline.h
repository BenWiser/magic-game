#ifndef _RENDER_PIPELINE_H
#define _RENDER_PIPELINE_H

#include <string>
#include <queue>
#include <memory>

#include "renderer/Renderer.h"

namespace RenderPipeline
{
using std::queue;
using std::shared_ptr;
using std::string;

struct RenderCommand
{
    string type;
    string fontPath;
    string text;
    unsigned int textureId;
    int shaderId;
    float width;
    float height;
    float x;
    float y;
    float angle;
    float maxFrames;
    float animations;
    float frame;
    float animation;
    float alpha;
    float depth;
    float r;
    float g;
    float b;
};

class PipeLine
{
private:
    queue<RenderCommand> _renderPipeline;

public:
    void enque(RenderCommand command);
    RenderCommand dequeue();
    bool hasNext();
    void render(Renderer &renderer);
    RenderCommand createRectColour(
        float r,
        float g,
        float b,
        float x,
        float y,
        float width,
        float height,
        float angle) const;
    RenderCommand createRect(
        unsigned int textureId,
        float x,
        float y,
        float width,
        float height,
        float angle) const;
    RenderCommand createSpriteMap(
        unsigned int textureId,
        float maxFrames,
        float animations,
        float frame,
        float animation,
        float x,
        float y,
        float width,
        float height,
        float angle) const;
    RenderCommand createText(
        string text,
        string fontPath,
        float x,
        float y,
        float size);
    RenderCommand setAlpha(
        const float &alpha);
    RenderCommand setDepth(
        const float &depth);
    RenderCommand setTiling(
        const float &horizontalTiling,
        const float &verticalTiling);
    RenderCommand setShader(
        int shaderId);
};

shared_ptr<PipeLine> getPipeLine();
}; // namespace RenderPipeline

#endif
#ifndef _VBO_H
#define _VBO_H

#include <vector>
#include <memory>

#include "renderer/shaders/Shader.h"

class VBO
{
private:
    unsigned int _vertexBufferId;
    unsigned int _textureBufferId;
    unsigned int _vertexCount;

public:
    VBO(std::vector<float> vertexs, std::vector<float> texCoords);
    ~VBO();
    void render(
        std::shared_ptr<Shader> shader,
        const float &frame,
        const float &animation,
        const float &uWidth,
        const float &vHeight,
        const float &alpha);
};

#endif
#ifndef _SHADER_MANAGER_H
#define _SHADER_MANAGER_H

#include <vector>
#include <memory>
#include <string>

#include "renderer/shaders/Shader.h"

namespace ShaderManager
{
using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

typedef int ShaderID;

ShaderID create(string vertexSource, string fragmentSource);
shared_ptr<Shader> getShader(const ShaderID &shaderId);
void setActiveShader(const ShaderID &shaderId);
shared_ptr<Shader> getActiveShader();

} // namespace ShaderManager

#endif
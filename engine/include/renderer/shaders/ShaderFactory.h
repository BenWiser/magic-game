#ifndef _SHADER_FACTORY_H
#define _SHADER_FACTORY_H

#include <memory>

#include "renderer/shaders/Shader.h"

namespace ShaderFactory
{
std::shared_ptr<Shader> createDefaultShader();

std::shared_ptr<Shader> createColourShader();

std::shared_ptr<Shader> createTextShader();
}; // namespace ShaderFactory

#endif
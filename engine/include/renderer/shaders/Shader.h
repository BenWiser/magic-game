#ifndef _SHADER_H
#define _SHADER_H

#include <string>
#include "openGL.h"

// TODO: Refactor shaders to be apart of the renderer
class Shader
{
private:
    GLuint _programId;
    GLuint compileShader(GLuint shaderType, const std::string &source);

public:
    Shader(const std::string &vertexSource, const std::string &FragmentSource);
    ~Shader();
    void useShader() const;
    void disableShader() const;
    unsigned int getAttribLocation(const std::string &attributeName);
    unsigned int getUniform(const std::string &uniformName);
};

#endif
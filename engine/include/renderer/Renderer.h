#ifndef _RENDERER_H
#define _RENDERER_H

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

#include "openGL.h"
#include "Vector2.h"
#include "renderer/VBO.h"

class Renderer
{
private:
    float _alpha;
    float _depth;
    float _horizontalTiling;
    float _verticalTiling;
    // TODO: Remove this window dependency
    GLFWwindow *_window;
    // TODO: Remove this VBO dependency - can probably use a singleton again like I did for shaders
    // TODO: Create renderer wrapper
    std::unordered_map<Vector2, std::shared_ptr<VBO>, Vector::hash_fn> _vbos;

    std::shared_ptr<VBO> _getVBO(const float &width, const float &height);

public:
    int initialise();

    void gameLoop(const std::function<void()> &loop);
    void terminate();
    void renderRectColour(
        const float &r,
        const float &g,
        const float &b,
        const float &x,
        const float &y,
        const float &width,
        const float &height,
        const float &angle);
    void renderRect(
        const uint32_t &textureId,
        const float &x,
        const float &y,
        const float &width,
        const float &height,
        const float &angle);
    void renderSpriteMap(
        const uint32_t &textureId,
        const float &maxFrames,
        const float &animations,
        const float &frame,
        const float &animation,
        const float &x,
        const float &y,
        const float &width,
        const float &height,
        const float &angle);
    void renderText(const std::string &text, const std::string& font, const float &x, const float &y, const float &fontSize);
    void setAlpha(const float &alpha);
    void setDepth(float depth);
    void setTiling(const float &horizontalTiling, const float &verticalTiling);
};

#endif
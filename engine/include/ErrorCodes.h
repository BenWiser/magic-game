#ifndef _ERROR_CODES_H
#define _ERROR_CODES_H

enum ErrorCodes
{
    SUCCESS = 0,
    FAILED_TO_INIT = 1,
    FAILED_TO_LOAD_IMAGE = 2,
    FAILED_TO_LOAD_SCRIPT = 3
};
#endif
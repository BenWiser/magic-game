#ifndef _GAME_STATE_H
#define _GAME_STATE_H

#include <string>

#include "Vector2.h"

namespace GameState
{
constexpr float MAX_FRAMERATE = 60.0f;
extern float WIDTH;
extern float HEIGHT;
extern float HALF_WIDTH;
extern float HALF_HEIGHT;
extern float VIEWPORT_WIDTH;
extern float VIEWPORT_HEIGHT;
extern int32_t VIEWPORT_OFFSET_X;
extern int32_t VIEWPORT_OFFSET_Y;
extern std::string GAME_NAME;
extern bool keyState[];
extern bool mouseState[];
extern long mouseScroll;
extern Vector2 mousePosition;
extern Vector2 cameraPosition;
extern float cameraZoom;

void init();
}; // namespace GameState

#endif
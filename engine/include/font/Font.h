#ifndef _FONT_H
#define _FONT_H

#include <string>
#include <memory>

#include "ft2build.h"
#include FT_FREETYPE_H

#include <map>

// TODO: Separate the fonts from opengl
#include "openGL.h"

class Font
{
private:
    FT_Face _face;
    std::map<char, uint32_t> _symbolMap;
    std::shared_ptr<FT_Bitmap> _getSymbolBitmap(const char &c);

public:
    Font(const FT_Library &freeTypeLibrary, const std::string &fontPath);
    uint32_t getSymbolTextureId(const char& c);
};

#endif
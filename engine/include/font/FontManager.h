#ifndef _FONT_MANAGER_H
#define _FONT_MANAGER_H

#include <memory>
#include <string>

#include "ft2build.h"
#include FT_FREETYPE_H

#include "font/Font.h"

namespace FontManager
{
class Manager
{
private:
    FT_Library _freeTypeLibrary;

public:
    Manager();
    std::shared_ptr<Font> loadFont(std::string fontPath);
};

std::shared_ptr<Manager> getFontManager();
} // namespace FontManager

#endif
#ifndef _PHYSICS_ENGINE_H
#define _PHYSICS_ENGINE_H

#include <memory>
#include <map>
#include <vector>
#include <set>

#include "Vector2.h"
#include "Box2D/Box2D.h"

namespace PhysicsEngine
{
using std::map;
using std::shared_ptr;

extern map<int, b2Body *> bodies;

shared_ptr<b2World> createWorld();
void iterate();
b2Body *createBody(
    const Vector2 &position,
    const Vector2 &dimensions,
    const bool &isDynamic,
    const bool &fixedRotation,
    const float& friction,
    const float &density,
    const bool &isSensor,
    int &bodyId);
void setGravity(const Vector2 &force);
void applyForce(const Vector2 &force, const int &bodyId);
void applyImpulse(const Vector2 &force, const int &bodyId);
void applyAngularImpulse(const float &angle, const int &bodyId);
void destroyBody(const int &bodyId);

/**
 * Perform a raytrace and the body id of the bodies collided with
 */
std::vector<int> rayTrace(const Vector2 &pointA, const Vector2 &pointB);

/**
 * Query for the physics bodies in the bounds
 */
std::vector<int> queryBounds(const Vector2 &pointA, const Vector2 &pointB);

/**
 * Return all the entities a body has collided with
 */
std::set<int> queryCollisions(const int &bodyId);

/**
 * Return if the two bodies have collided
 */
bool hasCollided(const int &bodyIdA, const int &bodyIdB);
} // namespace PhysicsEngine

#endif
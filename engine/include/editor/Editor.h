#ifndef _EDITOR_H
#define _EDITOR_H

#include <string>
#include <sstream>

// Bit of a nasty hack making this depend on opengl
// because the Glfw implementation needs to initialise
#include "openGL.h"
#include "scripting/ScriptEngine.h"

class Editor {
public:
    Editor(std::string& editorAssetDirectory);
    void registerAPIs(ScriptEngine& scriptEngine);
    void addMenuButton(std::string name, const std::function<void()> action);
    void loop();
    void shutdown();
    bool isEnabled();

private:
    std::ostringstream _output;
    std::streambuf* _cout_output;
    GLFWwindow* _window;
};

#endif //_EDITOR_H
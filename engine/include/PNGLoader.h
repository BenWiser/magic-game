#ifndef _PNG_LOADER_H
#define _PNG_LOADER_H

#include <string>

namespace PNGLoader
{
int loadPNG(std::string fileName, unsigned int &textureId);
void unloadTexture(const unsigned int &textureId);
}; // namespace PNGLoader

#endif
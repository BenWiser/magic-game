#ifndef _VECTOR_2_H
#define _VECTOR_2_H

#include <string>

struct Vector2
{
    float x;
    float y;
    Vector2(float X, float Y) : x(X), y(Y) {}
    bool operator==(const Vector2 &rhs) const
    {
        return ((int)x) == ((int)rhs.x) && ((int)y) == ((int)rhs.y);
    }
};

namespace Vector
{
struct hash_fn
{
    std::size_t operator()(const Vector2 &vector) const
    {
        std::string hash;
        hash += std::to_string((int)vector.x);
        hash += "00";
        hash += std::to_string((int)vector.y);
        return strtoul(hash.c_str(), NULL, 0);
    }
};
} // namespace Vector

#endif
#ifndef _SOUND_MANAGER_H
#define _SOUND_MANAGER_H

#include <memory>

#include "AL/al.h"
#include "AL/alc.h"

#include "sound/Sound.h"

namespace SoundManager
{
class Manager
{
private:
    ALCcontext *_context;
    ALCdevice *_device;
    ALuint _source;

public:
    Manager();
    void shutdown();
    void stopSound();
    void playSound(const Sound &sound);
};

std::shared_ptr<Manager> getSoundManager();
} // namespace SoundManager

#endif
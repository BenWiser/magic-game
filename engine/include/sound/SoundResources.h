#ifndef _SOUND_RESOURCES_H
#define _SOUND_RESOURCES_H

#include <vector>
#include <string>
#include <memory>

#include "sound/Sound.h"

/**
 * The sound resources namespaces manages all the active sounds that have been loaded
 * by the game script
 */

namespace SoundResources
{
using std::string;
using std::vector;
using std::shared_ptr;

typedef unsigned int soundId;

soundId loadSound(string fileName);

shared_ptr<Sound> getSound(soundId sound);

extern vector<shared_ptr<Sound>> sounds;
} // namespace SoundResources

#endif
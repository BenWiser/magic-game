#ifndef _SOUND_H
#define _SOUND_H

#include <memory>
#include <string>

#include "AL/al.h"
#include "AL/alc.h"

class Sound
{
public:
    Sound(std::string soundFile);
    ~Sound();
    ALuint bufferId;
};

#endif

#include <fstream>
#include <vector>

#include "FileReader.h"

namespace FileReader
{
using std::string;

string readFile(string fileName)
{
    using std::ifstream;
    using std::noskipws;
    using std::vector;

    ifstream file;
    file.open(fileName);

    if (!file.is_open())
    {
        return "";
    }

    char output;
    std::string data;
    while (!file.eof() && file >> noskipws >> output)
    {
        data.push_back(output);
    }

    file.close();

    return data;
}
} // namespace FileReader
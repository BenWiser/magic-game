#include "GameState.h"
#include "Configuration.h"

std::string GameState::GAME_NAME = "";

void GameState::init()
{
    Configuration configuration;
    GAME_NAME = configuration.getGameName();
}

float GameState::WIDTH = 1000;
float GameState::HEIGHT = 800;
float GameState::HALF_WIDTH = GameState::WIDTH / 2;
float GameState::HALF_HEIGHT = GameState::HEIGHT / 2;
float GameState::VIEWPORT_WIDTH = GameState::WIDTH;
float GameState::VIEWPORT_HEIGHT = GameState::HEIGHT;

int32_t GameState::VIEWPORT_OFFSET_X = 0;

int32_t GameState::VIEWPORT_OFFSET_Y = 0;

long GameState::mouseScroll = 0;

bool GameState::keyState[350];

bool GameState::mouseState[2];

Vector2 GameState::cameraPosition(GameState::HALF_WIDTH, GameState::HALF_HEIGHT);
float GameState::cameraZoom = 1.0f;

Vector2 GameState::mousePosition(0.0f, 0.0f);
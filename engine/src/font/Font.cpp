
#include "font/Font.h"
#include <iostream>

Font::Font(const FT_Library &freeTypeLibrary, const std::string &fontPath)
{
    FT_New_Face(freeTypeLibrary, fontPath.c_str(), 0, &_face);

    FT_Set_Pixel_Sizes(_face, 64, 0);

    FT_UInt index;
    FT_ULong character = FT_Get_First_Char(_face, &index);

    while (index)
    {
        const auto bitmap = _getSymbolBitmap(character);

        uint32_t fontTextureId;

        glGenTextures(1, &fontTextureId);
        glBindTexture(GL_TEXTURE_2D, fontTextureId);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA8, bitmap->width, bitmap->rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap->buffer);

        _symbolMap.insert(std::pair<char, uint32_t>(character, fontTextureId));

        character = FT_Get_Next_Char(_face, character, &index);
    }
}

std::shared_ptr<FT_Bitmap> Font::_getSymbolBitmap(const char &c)
{
    auto glyphIndex = FT_Get_Char_Index(_face, c);

    FT_Load_Glyph(_face, glyphIndex, FT_LOAD_DEFAULT);

    FT_Render_Glyph(_face->glyph, FT_RENDER_MODE_LCD);

    return std::make_shared<FT_Bitmap>(_face->glyph->bitmap);
}

uint32_t Font::getSymbolTextureId(const char &c)
{
    if (c == ' ' || !_symbolMap.count(c))
    {
        return 0;
    }
    return _symbolMap[c];
}

#include <map>

#include "font/FontManager.h"

namespace FontManager
{
std::shared_ptr<Manager> getFontManager()
{
    static auto manager = std::make_shared<Manager>();
    return manager;
}

Manager::Manager()
{
    FT_Init_FreeType(&_freeTypeLibrary);
}

std::shared_ptr<Font> Manager::loadFont(std::string fontPath)
{
    using std::make_shared;
    using std::map;
    using std::pair;
    using std::shared_ptr;
    using std::string;

    static map<string, shared_ptr<Font>> fontMap;

    if (fontMap.find(fontPath) == fontMap.end())
    {
        auto font = make_shared<Font>(_freeTypeLibrary, fontPath);
        fontMap.insert(pair<string, shared_ptr<Font>>(fontPath, font));
    }

    return fontMap[fontPath];
}
} // namespace FontManager
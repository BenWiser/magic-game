#include <vector>

#include "openGL.h"
#include "lodepng.h"
#include "ErrorCodes.h"
#include "PNGLoader.h"

namespace PNGLoader
{
using std::string;
using std::vector;

int loadPNG(string fileName, unsigned int &textureId)
{

    vector<unsigned char> image;
    unsigned int width;
    unsigned int height;
    unsigned int error = lodepng::decode(image, width, height, fileName.c_str());

    if (error != 0)
    {
        return ErrorCodes::FAILED_TO_LOAD_IMAGE;
    }

    // TODO: Make this handled by the renderer
    glGenTextures(1, &textureId);

    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

    return 0;
}

void unloadTexture(const unsigned int &textureId)
{
    glDeleteTextures(1, &textureId);
}
} // namespace PNGLoader
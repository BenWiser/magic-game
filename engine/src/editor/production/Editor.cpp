#include "editor/Editor.h"
#include "GameState.h"

#include "openGL.h"

Editor::Editor(std::string& editorAssetDirectory)
{
    _window = glfwGetCurrentContext();
}

void Editor::registerAPIs(ScriptEngine& scriptEngine)
{

}

void Editor::addMenuButton(std::string name, const std::function<void()> action)
{
}

void Editor::loop()
{
    // TODO: Get rid of this hackiness
    // The renderer should resize itself if the editor is not enabled
    // This can be fixed if the issues with the viewport y being diffferent
    // from what is set for the gamestate offset y is fixed
    // It is like that because of the mouse
    int window_width;
    int window_height;

    glfwGetWindowSize(_window, &window_width, &window_height);

    static auto ratio = GameState::VIEWPORT_WIDTH / GameState::VIEWPORT_HEIGHT;

    if (window_height < window_width)
    {
        GameState::VIEWPORT_HEIGHT = window_height;
        GameState::VIEWPORT_WIDTH = GameState::VIEWPORT_HEIGHT * ratio;
    }
    else
    {
        GameState::VIEWPORT_WIDTH = window_width;
        GameState::VIEWPORT_HEIGHT = GameState::VIEWPORT_WIDTH / ratio;
    }

    auto viewport_x = (window_width / 2.0f) - (GameState::VIEWPORT_WIDTH / 2);
    auto viewport_y = (window_height / 2.0f) - (GameState::VIEWPORT_HEIGHT / 2);

    GameState::VIEWPORT_OFFSET_X = viewport_x;
    GameState::VIEWPORT_OFFSET_Y = window_height - viewport_y - GameState::VIEWPORT_HEIGHT;

    glViewport(
        viewport_x,
        viewport_y,
        GameState::VIEWPORT_WIDTH,
        GameState::VIEWPORT_HEIGHT
    );
}

void Editor::shutdown()
{
}

bool Editor::isEnabled()
{
    return false;
}
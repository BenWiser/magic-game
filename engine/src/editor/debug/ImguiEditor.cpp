#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <variant>
#include <sstream>
#include <unordered_map>

#include "editor/Editor.h"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "GameState.h"

namespace editor
{
    constexpr int width = 350;
    constexpr int height = 200;
    constexpr int console_height = 200;
    constexpr int menu_height = 26;
    ImVec4 border_colour(0.2f, 0.2f, 0.2f, 1.0f);
    ImVec4 text_colour(0.6f, 0.6f, 0.6f, 1.00f);
    auto primary_colour = IM_COL32(5, 147, 240, 255);

    constexpr int tabbar_height = 26;
    std::string asset_directory;

    bool first_tab;
    std::string current_tab = "";
    std::unordered_map<std::string, std::string> tabs_selected;
    std::function<void(std::variant<unsigned int, double, bool, std::string, std::nullptr_t>)> tab_callback = nullptr;

    struct Section
    {
        std::string type;
        std::string name;
        std::vector<std::variant<unsigned int, double, bool, std::string>> values;
        std::function<void(std::variant<unsigned int, double, bool, std::string, std::nullptr_t>)> callback;
    };

    struct EditorMenuButton {
        std::string name;
        const std::function<void()> action;

        EditorMenuButton(
            std::string n,
            const std::function<void()> a
        ): name(n), action(a) {}
    };

    std::vector<Section> sections;
    std::vector<std::unique_ptr<EditorMenuButton>> buttons;

    int get_asset_directory(lua_State* state)
    {
        lua_pushstring(state, asset_directory.c_str());
        return 1;
    }

    int begin_menu(lua_State* state)
    {
        sections.push_back(Section {
            "begin_menu"
        });
        return 0;
    }

    int end_menu(lua_State* state)
    {
        sections.push_back(Section {
            "end_menu"
        });
        return 0;
    }

    int begin_center(lua_State* state)
    {
        sections.push_back(Section {
            "begin_center"
        });
        return 0;
    }

    int begin_viewport_top_section(lua_State* state)
    {
        sections.push_back(Section {
            "begin_viewport_top_section"
        });
        return 0;
    }

    int begin_status(lua_State* state)
    {
        sections.push_back(Section {
            "begin_status"
        });
        return 0;
    }

    int begin_group(lua_State* state)
    {
        sections.push_back(Section {
            "begin_group",
        });
        return 0;
    }

    int begin_sidebar_child(lua_State* state)
    {
        auto name = lua_tostring(state, 1);
        sections.push_back(Section {
            "begin_sidebar_child",
            name
        });
        return 0;
    }

    int begin_sidebar(lua_State* state)
    {
        sections.push_back(Section {
            "begin_sidebar"
        });
        return 0;
    }

    int begin_tab_bar(lua_State* state)
    {
        auto name = lua_tostring(state, 1);
        sections.push_back(Section {
            "begin_tab_bar",
            name
        });
        return 0;
    }

    int end(lua_State* state)
    {
        sections.push_back(Section {
            "end"
        });
        return 0;
    }

    int end_sidebar_child(lua_State* state)
    {
        sections.push_back(Section {
            "end_sidebar_child"
        });
        return 0;
    }

    int end_sidebar(lua_State* state)
    {
        sections.push_back(Section {
            "end_sidebar"
        });
        return 0;
    }

    int end_group(lua_State* state)
    {
        sections.push_back(Section {
            "end_group"
        });
        return 0;
    }

    int end_tab_bar(lua_State* state)
    {
        sections.push_back(Section {
            "end_tab_bar"
        });
        return 0;
    }

    int same_line(lua_State* state)
    {
        sections.push_back(Section {
            "same_line"
        });
        return 0;
    }

    int separator(lua_State* state)
    {
        sections.push_back(Section {
            "separator"
        });
        return 0;
    }

    int text(lua_State* state)
    {
        auto text = lua_tostring(state, 1);
        sections.push_back(Section {
            "text",
            text
        });
        return 0;
    }

    int input_string(lua_State* state)
    {
        auto variable_name = lua_tostring(state, 1);
        auto string = std::string(lua_tostring(state, 2));
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "input_string",
            variable_name,
            {string},
            [state, reference](auto string) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pushstring(state, std::get<std::string>(string).c_str());
                lua_pcall(state, 1, 0, 0);
            }
        });
        return 0;
    }

    int input_number(lua_State* state)
    {
        auto variable_name = lua_tostring(state, 1);
        auto number = lua_tonumber(state, 2);
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "input_number",
            variable_name,
            {number},
            [state, reference](auto number) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pushnumber(state, std::get<double>(number));
                lua_pcall(state, 1, 0, 0);
            }
        });
        return 0;
    }

    int input_checkbox(lua_State* state)
    {
        auto variable_name = lua_tostring(state, 1);
        auto boolean = static_cast<bool>(lua_toboolean(state, 2));
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "input_checkbox",
            variable_name,
            {boolean},
            [state, reference](auto boolean) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pushboolean(state, std::get<bool>(boolean));
                lua_pcall(state, 1, 0, 0);
            }
        });
        return 0;
    }

    int register_menu_button(lua_State* state)
    {
        auto button_name = lua_tostring(state, 1);
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "menu_button",
            button_name,
            {},
            [state, reference](auto _) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pcall(state, 0, 0, 0);
            }
        });
        return 0;
    }

    int register_tab_button(lua_State* state)
    {
        auto button_name = lua_tostring(state, 1);
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "tab_button",
            button_name,
            {},
            [state, reference](auto _) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pcall(state, 0, 0, 0);
            }
        });
        return 0;
    }

    int register_button(lua_State* state)
    {
        auto button_name = lua_tostring(state, 1);
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "button",
            button_name,
            {},
            [state, reference](auto _) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pcall(state, 0, 0, 0);
            }
        });
        return 0;
    }

    int register_image_button(lua_State* state)
    {
        auto name = lua_tostring(state, 1);
        auto texture_index = static_cast<unsigned int>(lua_tointeger(state, 2));
        auto image_size = static_cast<double>(lua_tonumber(state, 3));
        auto uv0_x = static_cast<double>(lua_tonumber(state, 4));
        auto uv0_y = static_cast<double>(lua_tonumber(state, 5));
        auto uv1_x = static_cast<double>(lua_tonumber(state, 6));
        auto uv1_y = static_cast<double>(lua_tonumber(state, 7));
        auto reference = luaL_ref(state, LUA_REGISTRYINDEX);

        sections.push_back(Section {
            "image_button",
            name,
            {
                texture_index,
                image_size,
                uv0_x,
                uv0_y,
                uv1_x,
                uv1_y,
            },
            [state, reference](auto _) {
                lua_rawgeti(state, LUA_REGISTRYINDEX, reference);
                lua_pcall(state, 0, 0, 0);
            },
        });
        return 0;
    }

    /**
     *  Render a section with ImGui
     *  @return {bool} return if it is fine to continue showing sections
     */
    bool render_section(Section& section, int window_width, int window_height, int viewport_x)
    {
        constexpr int section_height = 200;
        static ImVec2 child_size(editor::width - 30, section_height);

        if (section.type == "begin_menu")
        {
            ImGui::BeginMainMenuBar();
            ImGui::TextColored(ImVec4(0.4, 0.4, 0.4, 1.0), ENGINE_VERSION);
            for (const auto& button : buttons)
            {
                if (ImGui::MenuItem(button->name.c_str()))
                {
                    button->action();

                    // TODO: Get rid of this dirty hack
                    // The tldr is that when the game is restarted
                    // the sections below are no longer valid and can cause a sigfault if they
                    // try to access memory that is no longer valid (can happen when setting values)
                    // This was a nasty way to fix this but it means any project buttons added
                    // would clear the buttons which is super undesirable
                    ImGui::EndMainMenuBar();
                    return false;
                }
            }
        }

        if (section.type == "end_menu")
        {
            ImGui::EndMainMenuBar();
        }

        if (section.type == "menu_button" && ImGui::MenuItem(section.name.c_str()))
        {
            section.callback(nullptr);
        }

        if (section.type == "begin_tab_bar")
        {
            current_tab = section.name.c_str();
            first_tab = true;
        }

        if (section.type == "begin_group")
        {
            ImGui::BeginGroup();
        }

        if (section.type == "end_group")
        {
            ImGui::EndGroup();
        }        

        if (section.type == "end_tab_bar")
        {
            auto draw_list = ImGui::GetWindowDrawList();
            auto cursor = ImGui::GetCursorScreenPos();
            draw_list->AddRectFilled(
                ImVec2(cursor.x, cursor.y - 9),
                ImVec2(cursor.x + editor::width, cursor.y - 7),
                IM_COL32(24, 114, 173, 255)
            );

            if (tab_callback != nullptr)
            {
                // We want to show a new set of sections so empty the sections
                // render them, and then switch back to what they were before
                auto sections_before = editor::sections;
                editor::sections = {};
                tab_callback(nullptr);

                for (auto &section : editor::sections)
                {
                    editor::render_section(section, window_width, window_height, viewport_x);
                }

                editor::sections = sections_before;
                current_tab = "";
            }

            tab_callback = nullptr;
        }

        if (section.type == "begin_sidebar_child")
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(1.0f, 0.0f));
            auto draw_list = ImGui::GetWindowDrawList();
            auto cursor = ImGui::GetCursorScreenPos();
            draw_list->AddRectFilled(
                ImVec2(cursor.x - 20, cursor.y - 5),
                ImVec2(cursor.x + editor::width, cursor.y + 25),
                editor::primary_colour
            );
            auto& io = ImGui::GetIO();
            ImGui::PushFont(io.Fonts->Fonts[1]);
            ImGui::Text(section.name.c_str());
            ImGui::PopFont();
            ImGui::PopStyleVar();

            ImGui::Spacing();

            ImGui::BeginChild(section.name.c_str(), child_size);
        }

        if (section.type == "begin_viewport_top_section")
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
            ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
            ImGui::SetNextWindowPos(
                ImVec2(
                    viewport_x,
                    GameState::VIEWPORT_OFFSET_Y - tabbar_height
                )
            );
            ImGui::Begin("viewport_top_section", 0, ImGuiWindowFlags_NoResize
                | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar
                | ImGuiWindowFlags_AlwaysAutoResize);
            ImGui::PopStyleVar();
            ImGui::PopStyleColor();
        }

        if (section.type == "begin_center")
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(5, 5));
            ImGui::SetNextWindowPos(
                ImVec2(
                    (GameState::VIEWPORT_WIDTH / 2) + viewport_x - 20,
                    GameState::VIEWPORT_OFFSET_Y + 5
                )
            );
            ImGui::Begin("center", 0, ImGuiWindowFlags_NoResize
                | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar
                | ImGuiWindowFlags_AlwaysAutoResize);
            ImGui::PopStyleVar();
        }

        if (section.type == "begin_sidebar")
        {
            ImGui::PushStyleColor(ImGuiCol_Border, editor::border_colour);
            ImGui::SetNextWindowSize(ImVec2(editor::width, window_height - editor::menu_height));
            ImGui::SetNextWindowPos(ImVec2(window_width - editor::width, editor::menu_height));
            ImGui::Begin("Sidebar", 0,
                        ImGuiWindowFlags_NoResize
                        | ImGuiWindowFlags_NoCollapse
                        | ImGuiWindowFlags_NoTitleBar);
            ImGui::PopStyleColor();
        }

        if (section.type == "begin_status")
        {
            ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.06f, 0.05f, 0.07f, 0.7f));
            constexpr int8_t statusHeight = 46;
            ImGui::SetNextWindowPos(
                ImVec2(GameState::VIEWPORT_OFFSET_X, GameState::VIEWPORT_HEIGHT + GameState::VIEWPORT_OFFSET_Y - statusHeight)
            );
            ImGui::Begin("status", 0, ImGuiWindowFlags_NoResize
                | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar
                | ImGuiWindowFlags_AlwaysAutoResize);
            ImGui::PopStyleColor();
        }

        if (section.type == "button" && ImGui::Button(section.name.c_str()))
        {
            section.callback(nullptr);
        }

        if (section.type == "tab_button")
        {
            if (!tabs_selected.count(current_tab))
            {
                tabs_selected[current_tab] = section.name.c_str();
            }
            
            if (first_tab)
            {
                first_tab = false;
            }
            else
            {
                ImGui::SameLine();
            }

            static ImVec4 tab_colour(0.094f, 0.44f, 0.68, 1.00f);
            static ImVec4 tab_hovered(0.078f, 0.51f, 0.8, 1.00f);
            auto& io = ImGui::GetIO();

            bool tab_selected = tabs_selected[current_tab] == section.name.c_str();
            if (tab_selected)
            {
                ImGui::PushStyleColor(ImGuiCol_Button, tab_colour);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, tab_colour);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, tab_hovered);
                tab_callback = section.callback;
            }
            ImGui::PushFont(io.Fonts->Fonts[2]);
            if (ImGui::Button(section.name.c_str()))
            {
                tabs_selected[current_tab] = section.name.c_str();
                tab_callback = section.callback;
            }
            if (tab_selected)
            {
                ImGui::PopStyleColor();
                ImGui::PopStyleColor();
                ImGui::PopStyleColor();
            }
            ImGui::PopFont();
        }

        if (section.type == "image_button")
        {
            ImGui::PushID(section.name.c_str());
            if (ImGui::ImageButton(
                (void*) (intptr_t) std::get<unsigned int>(section.values[0]),
                ImVec2(std::get<double>(section.values[1]), std::get<double>(section.values[1])),
                ImVec2(std::get<double>(section.values[2]), std::get<double>(section.values[3])),
                ImVec2(std::get<double>(section.values[4]), std::get<double>(section.values[5]))
            ))
            {
                section.callback(nullptr);
            }
            ImGui::PopID();
        }

        if (section.type == "same_line")
        {
            ImGui::SameLine();
        }

        if (section.type == "separator")
        {
            ImGui::Separator();
        }

        if (section.type == "input_string")
        {
            static char text_buffer[32];
            auto& value = std::get<std::string>(section.values[0]);
            strcpy(text_buffer, value.data());

            ImGui::PushID(section.name.c_str());
            ImGui::InputText("", text_buffer, 32);
            ImGui::PopID();
            ImGui::SameLine();
            ImGui::PushStyleColor(ImGuiCol_Text, editor::text_colour);
            ImGui::Text(section.name.c_str());
            ImGui::PopStyleColor();

            section.callback(std::string(text_buffer));
        }

        if (section.type == "input_checkbox")
        {
            auto value = std::get<bool>(section.values[0]);
            ImGui::PushID(section.name.c_str());
            ImGui::Checkbox("", &value);
            ImGui::PopID();
            ImGui::SameLine();
            ImGui::PushStyleColor(ImGuiCol_Text, editor::text_colour);
            ImGui::Text(section.name.c_str());
            ImGui::PopStyleColor();
            section.callback(value);
        }

        if (section.type == "input_number")
        {
            auto& value = std::get<double>(section.values[0]);
            ImGui::PushID(section.name.c_str());
            ImGui::InputDouble("", &value, 0.1f, 1.0f, "%.8f");
            ImGui::PopID();
            ImGui::SameLine();
            ImGui::PushStyleColor(ImGuiCol_Text, editor::text_colour);
            ImGui::Text(section.name.c_str());
            ImGui::PopStyleColor();
            section.callback(value);
        }

        if (section.type == "text")
        {
            ImGui::PushStyleColor(ImGuiCol_Text, editor::text_colour);
            ImGui::Text(section.name.c_str());
            ImGui::PopStyleColor();
        }

        if (section.type == "end")
        {
            ImGui::End();
        }

        if (section.type == "end_sidebar_child")
        {
            ImGui::EndChild();
        }

        if (section.type == "end_sidebar")
        {
            ImGui::End();
        }
        return true;
    }

    void set_theme()
    {
        ImGuiStyle * style = &ImGui::GetStyle();

        style->WindowPadding = ImVec2(15, 15);
        style->FramePadding = ImVec2(5, 5);
        style->ItemSpacing = ImVec2(12, 8);
        style->ItemInnerSpacing = ImVec2(8, 6);
        style->IndentSpacing = 25.0f;
        style->ScrollbarSize = 15.0f;
        style->ScrollbarRounding = 9.0f;
        style->GrabMinSize = 5.0f;
        style->GrabRounding = 3.0f;
    
        style->Colors[ImGuiCol_Text] = ImVec4(0.80f, 0.80f, 0.83f, 1.00f);
        style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
        style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
        style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
        style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
        style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
        style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
        style->Colors[ImGuiCol_TitleBg] = ImVec4(0.012f, 0.58f, 0.94f, 1.00f);
        style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.012f, 0.58f, 0.94f, 1.00f);
        style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.1f, 0.1f, 0.1f, 1.0f);
        style->Colors[ImGuiCol_Border] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
        style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
        style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
        style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
        style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
        style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
        style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
        style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
        style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
        style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
        style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
        style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
        style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
        style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
        style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);
        style->Colors[ImGuiCol_Tab] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
        style->Colors[ImGuiCol_TabHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
        style->Colors[ImGuiCol_TabActive] = ImVec4(0.19f, 0.17f, 0.20f, 1.00f);
    }
}

Editor::Editor(std::string& editorAssetDirectory)
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();

    editor::asset_directory = editorAssetDirectory;

    _window = glfwGetCurrentContext();

    int width;
    int height;
    int x_pos;
    int y_pos;

    glfwGetWindowPos(_window, &x_pos, &y_pos);
    x_pos -= editor::width / 2;
    y_pos -= editor::height / 2 + editor::menu_height / 2;
    glfwSetWindowPos(_window, x_pos, y_pos);

    glfwGetWindowSize(_window, &width, &height);

    glfwSetWindowSize(_window, width + editor::width, height + editor::height + editor::menu_height);

    auto clearColour = 0.1f;
    glClearColor(clearColour, clearColour, clearColour, 1.0f);

    ImGui_ImplGlfw_InitForOpenGL(_window, true);
    ImGui_ImplOpenGL3_Init("#version 110");

    editor::set_theme();

    _cout_output = std::cout.rdbuf();
    std::cout.rdbuf(_output.rdbuf());

    io.Fonts->AddFontFromFileTTF(
        (editorAssetDirectory + "/fonts/Ubuntu-Regular.ttf").c_str(),
        16
    );

    io.Fonts->AddFontFromFileTTF(
        (editorAssetDirectory + "/fonts/Ubuntu-Bold.ttf").c_str(),
        18
    );

    io.Fonts->AddFontFromFileTTF(
        (editorAssetDirectory + "/fonts/Ubuntu-Bold.ttf").c_str(),
        16
    );
}

void Editor::registerAPIs(ScriptEngine& scriptEngine)
{
    scriptEngine.addAPI("Magic_editor_registerButton", editor::register_button);
    scriptEngine.addAPI("Magic_editor_registerImageButton", editor::register_image_button);
    scriptEngine.addAPI("Magic_edtior_inputString", editor::input_string);
    scriptEngine.addAPI("Magic_editor_inputNumber", editor::input_number);
    scriptEngine.addAPI("Magic_editor_inputCheckbox", editor::input_checkbox);
    scriptEngine.addAPI("Magic_editor_beginGroup", editor::begin_group);
    scriptEngine.addAPI("Magic_editor_beginSidebarChild", editor::begin_sidebar_child);
    scriptEngine.addAPI("Magic_editor_beginSidebar", editor::begin_sidebar);
    scriptEngine.addAPI("Magic_editor_end", editor::end);
    scriptEngine.addAPI("Magic_editor_endGroup", editor::end_group);
    scriptEngine.addAPI("Magic_editor_endSidebarChild", editor::end_sidebar_child);
    scriptEngine.addAPI("Magic_editor_endSidebar", editor::end_sidebar);
    scriptEngine.addAPI("Magic_editor_sameLine", editor::same_line);
    scriptEngine.addAPI("Magic_editor_text", editor::text);
    scriptEngine.addAPI("Magic_editor_separator", editor::separator);
    scriptEngine.addAPI("Magic_editor_beginCenter", editor::begin_center);
    scriptEngine.addAPI("Magic_editor_beginStatus", editor::begin_status);
    scriptEngine.addAPI("Magic_editor_getAssetDirectory", editor::get_asset_directory);
    scriptEngine.addAPI("Magic_editor_beginMenu", editor::begin_menu);
    scriptEngine.addAPI("Magic_editor_endMenu", editor::end_menu);
    scriptEngine.addAPI("Magic_editor_registerMenuButton", editor::register_menu_button);
    scriptEngine.addAPI("Magic_editor_beginTabBar", editor::begin_tab_bar);
    scriptEngine.addAPI("Magic_editor_endTabBar", editor::end_tab_bar);
    scriptEngine.addAPI("Magic_editor_registerTabButton", editor::register_tab_button);
    scriptEngine.addAPI("Magic_editor_beginViewportTopSection", editor::begin_viewport_top_section);
}

void Editor::addMenuButton(std::string name, const std::function<void()> action)
{
    editor::buttons.push_back(std::make_unique<editor::EditorMenuButton>(name, action));
}

void Editor::loop()
{
    int window_width;
    int window_height;

    glfwGetWindowSize(_window, &window_width, &window_height);

    static auto ratio = GameState::VIEWPORT_WIDTH / GameState::VIEWPORT_HEIGHT;

    const auto view_width = window_width - editor::width;
    const auto view_height = window_height
        - editor::console_height
        - editor::menu_height
        - editor::tabbar_height;

    if (view_height < view_width)
    {
        GameState::VIEWPORT_HEIGHT = view_height;
        GameState::VIEWPORT_WIDTH = GameState::VIEWPORT_HEIGHT * ratio;
    }
    else
    {
        GameState::VIEWPORT_WIDTH = view_width;
        GameState::VIEWPORT_HEIGHT = GameState::VIEWPORT_WIDTH / ratio;
    }

    auto viewport_x = (view_width / 2.0f) - (GameState::VIEWPORT_WIDTH / 2);
    auto viewport_y = editor::height
        + (view_height / 2.0f) - (GameState::VIEWPORT_HEIGHT / 2);

    GameState::VIEWPORT_OFFSET_X = viewport_x;
    GameState::VIEWPORT_OFFSET_Y = window_height - viewport_y - GameState::VIEWPORT_HEIGHT;

    glViewport(
        viewport_x,
        viewport_y,
        GameState::VIEWPORT_WIDTH,
        GameState::VIEWPORT_HEIGHT
    );

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    for (auto& section : editor::sections)
    {
        if (!editor::render_section(section, window_width, window_height, viewport_x))
        {
            goto end_sections;
        }
    }

    end_sections:

    // The script engine editor follows a similar API to im gui
    // so clear the sections out after they have been rendered
    editor::sections.clear();

    ImGui::PushStyleColor(ImGuiCol_Border, editor::border_colour);
    ImGui::SetNextWindowSize(ImVec2(
        window_width - editor::width,
        editor::console_height
    ));
    ImGui::SetNextWindowPos(ImVec2(0, window_height - editor::console_height));
    auto& io = ImGui::GetIO();
    ImGui::PushFont(io.Fonts->Fonts[1]);
    ImGui::Begin("Output", 0, ImGuiWindowFlags_NoResize
        | ImGuiWindowFlags_NoCollapse);
    ImGui::PopFont();
    ImGui::PopStyleColor();

    std::string line;
    std::stringstream stream(_output.str());
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(12, 2));
    ImGui::PushFont(io.Fonts->Fonts[0]);
    while(std::getline(stream, line))
    {
        ImGui::Text(line.c_str());
    }
    ImGui::PopFont();
    ImGui::PopStyleVar();

    ImGui::End();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Editor::shutdown()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    std::cout.rdbuf(_cout_output);
}

bool Editor::isEnabled()
{
    return true;
}
#include <math.h>
#include <vector>

#include "openGL.h"
#include "GameState.h"
#include "ErrorCodes.h"
#include "renderer/Renderer.h"
#include "renderer/VBO.h"
#include "renderer/shaders/ShaderFactory.h"
#include "renderer/shaders/ShaderManager.h"
#include "font/FontManager.h"

int Renderer::initialise()
{
    if (!glfwInit())
    {
        return ErrorCodes::FAILED_TO_INIT;
    }

    _window = glfwCreateWindow(GameState::WIDTH, GameState::HEIGHT, GameState::GAME_NAME.c_str(), NULL, NULL);

    if (!_window)
    {
        glfwTerminate();
        return ErrorCodes::FAILED_TO_INIT;
    }

    glfwMakeContextCurrent(_window);

    if (glewInit() != GLEW_OK)
    {
        glfwTerminate();
        return ErrorCodes::FAILED_TO_INIT;
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, GameState::WIDTH, GameState::HEIGHT, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW_MATRIX);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthFunc(GL_LEQUAL);

    glfwSetKeyCallback(_window, [](GLFWwindow *window, int key, int scanCode, int action, int mods) {
        if (key >= 0 && key < 350)
        {
            GameState::keyState[key] = action > 0;
        }
    });

    glfwSetMouseButtonCallback(_window, [](GLFWwindow *window, int button, int action, int mods) {
        GameState::mouseState[button] = action > 0;
    });

    glfwSetScrollCallback(_window, [](GLFWwindow* window, double xoffset, double yoffset) {
        GameState::mouseScroll += yoffset;
    });

    _alpha = 1.0f;
    _horizontalTiling = 1.0f;
    _verticalTiling = 1.0f;
    _depth = 0.0f;

    return 0;
}

void Renderer::gameLoop(const std::function<void()> &loop)
{
    while (!glfwWindowShouldClose(_window))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        double mouseX;
        double mouseY;
        glfwGetCursorPos(_window, &mouseX, &mouseY);
        GameState::mousePosition.x = static_cast<float>(mouseX);
        GameState::mousePosition.y = static_cast<float>(mouseY);

        glPushMatrix();
        glTranslatef(GameState::HALF_WIDTH, GameState::HALF_HEIGHT, 0.0f);
        glScalef(GameState::cameraZoom, GameState::cameraZoom, 1.0f);
        glTranslatef(-GameState::cameraPosition.x, -GameState::cameraPosition.y, 0.0f);

        loop();

        glPopMatrix();

        glfwSwapBuffers(_window);
        glfwPollEvents();
    }
}

std::shared_ptr<VBO> Renderer::_getVBO(const float &width, const float &height)
{
    Vector2 vboKey(width, height);
    const auto vboIndex = _vbos.find(vboKey);
    if (vboIndex == _vbos.end())
    {
        auto pair = std::make_pair(
            vboKey,
            std::make_shared<VBO>(
                std::vector<float>{
                    -width / 2, -height / 2,
                    width /2 , -height / 2,
                    -width / 2, height / 2,
                    width / 2, height / 2},
                std::vector<float>{
                    0.0f, 0.0f,
                    1.0, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f}));
        _vbos.insert(pair);
    }
    const auto vbo = _vbos.at(vboKey);

    return vbo;
}

void Renderer::renderRectColour(
        const float &r,
        const float &g,
        const float &b,
        const float &x,
        const float &y,
        const float &width,
        const float &height,
        const float &angle)
{
    const auto vbo = _getVBO(width, height);

    std::shared_ptr<Shader> currentShader = nullptr;

    if ((currentShader = ShaderManager::getActiveShader()) == nullptr)
    {
        currentShader = ShaderFactory::createColourShader();
    }

    currentShader->useShader();

    glColor3f(r, g, b);
    glPushMatrix();
    glTranslatef(x, y, _depth);
    glRotatef(angle * 180 / M_PI, 0.0f, 0.0f, 1.0f);

    vbo->render(
        currentShader,
        1,
        1,
        1,
        1,
        _alpha);

    glPopMatrix();
    glColor3f(1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, 0);

    currentShader->disableShader();
}

void Renderer::renderRect(
    const uint32_t &textureId,
    const float &x,
    const float &y,
    const float &width,
    const float &height,
    const float &angle)
{
    renderSpriteMap(textureId, 1, 1, 0, 0, x, y, width, height, angle);
}

void Renderer::renderSpriteMap(
    const uint32_t &textureId,
    const float &maxFrames,
    const float &animations,
    const float &frame,
    const float &animation,
    const float &x,
    const float &y,
    const float &width,
    const float &height,
    const float &angle)
{
    const auto vbo = _getVBO(width, height);

    auto ANIMATION_WIDTH = 1.0f / maxFrames;
    auto ANIMATION_HEIGHT = 1.0f / animations;

    std::shared_ptr<Shader> currentShader = nullptr;

    if ((currentShader = ShaderManager::getActiveShader()) == nullptr)
    {
        currentShader = ShaderFactory::createDefaultShader();
    }

    currentShader->useShader();

    glBindTexture(GL_TEXTURE_2D, textureId);
    glPushMatrix();
    glTranslatef(x, y, _depth);
    glRotatef(angle * 180 / M_PI, 0.0f, 0.0f, 1.0f);

    vbo->render(
        currentShader,
        ANIMATION_WIDTH * frame,
        ANIMATION_HEIGHT * animation,
        ANIMATION_WIDTH * _horizontalTiling,
        ANIMATION_HEIGHT * _verticalTiling,
        _alpha);

    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    currentShader->disableShader();
}

void Renderer::renderText(const std::string &text, const std::string& font, const float &x, const float &y, const float &fontSize)
{
    const auto fontManager = FontManager::getFontManager();
    const auto pixelFont = fontManager->loadFont(font);
    const auto textShader = ShaderFactory::createTextShader();

    const auto vbo = _getVBO(fontSize, fontSize);

    textShader->useShader();

    glPushMatrix();
    glTranslatef(x, y, _depth);
    for (const auto &c : text)
    {
        auto symbolTexture = pixelFont->getSymbolTextureId(c);
        if (symbolTexture != 0)
        {
            glTranslatef(fontSize * 2, 0.0f, 0.0f);
            glBindTexture(GL_TEXTURE_2D, pixelFont->getSymbolTextureId(c));
            vbo->render(
                textShader,
                1,
                1,
                fontSize,
                fontSize,
                _alpha);
        }
        else
        {
            glTranslatef(fontSize, 0.0f, 0.0f);
        }
    }
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    textShader->disableShader();
}

void Renderer::setAlpha(const float &alpha)
{
    _alpha = alpha;
}

void Renderer::setDepth(float depth)
{
    _depth = depth;
}

void Renderer::setTiling(const float &horizontalTiling, const float &verticalTiling)
{
    _horizontalTiling = horizontalTiling;
    _verticalTiling = verticalTiling;
}

void Renderer::terminate()
{
    glfwDestroyWindow(_window);
    glfwTerminate();
}
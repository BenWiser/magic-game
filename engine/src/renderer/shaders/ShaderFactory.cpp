
#include "renderer/shaders/ShaderFactory.h"

std::shared_ptr<Shader> ShaderFactory::createDefaultShader()
{
    static auto defaultShader = std::make_shared<Shader>(
        "#version 110\n"
        "attribute vec2 vertexPosition;"
        "attribute vec2 texturePosition;"
        "varying vec2 out_texturePosition;"
        ""
        "void main() {"
        "   gl_Position = gl_ModelViewProjectionMatrix * vec4(vertexPosition, 0.0, 1.0);"
        "   out_texturePosition = texturePosition;"
        "}",

        "#version 110\n"
        "uniform sampler2D texture;"
        "uniform float alpha;"
        "uniform vec2 uvDimenions;"
        "uniform vec2 animationInformation;"
        "varying vec2 out_texturePosition;"
        ""
        "void main() {"
        "   gl_FragColor = texture2D("
        "           texture,"
        "           out_texturePosition * uvDimenions + animationInformation"
        "   ) "
        "   * vec4(1.0, 1.0, 1.0, alpha);"
        "}");
    return defaultShader;
}

std::shared_ptr<Shader> ShaderFactory::createColourShader()
{
    static auto colourShader = std::make_shared<Shader>(
        "#version 110\n"
        "attribute vec2 vertexPosition;"
        "attribute vec2 texturePosition;"
        "varying vec3 out_colour;"
        ""
        "void main() {"
        "   gl_Position = gl_ModelViewProjectionMatrix * vec4(vertexPosition, 0.0, 1.0);"
        "   out_colour = gl_Color.rgb;"
        "}",

        "#version 110\n"
        "uniform sampler2D texture;"
        "uniform float alpha;"
        "uniform vec2 uvDimenions;"
        "uniform vec2 animationInformation;"
        "varying vec3 out_colour;"
        ""
        "void main() {"
        "   gl_FragColor = vec4(out_colour, alpha);"
        "}");
    return colourShader;
}

std::shared_ptr<Shader> ShaderFactory::createTextShader()
{
    static auto textShader = std::make_shared<Shader>(
        "#version 110\n"
        "attribute vec2 vertexPosition;"
        "attribute vec2 texturePosition;"
        "varying vec2 out_texturePosition;"
        ""
        "void main() {"
        "   gl_Position = gl_ModelViewProjectionMatrix * vec4(vertexPosition, 0.0, 1.0);"
        "   out_texturePosition = texturePosition;"
        "}",

        "#version 110\n"
        "uniform sampler2D texture;"
        "uniform float alpha;"
        "uniform vec2 uvDimenions;"
        "uniform vec2 animationInformation;"
        "varying vec2 out_texturePosition;"
        ""
        "void main() {"
        "   gl_FragColor = vec4(1.0, 1.0, 1.0, texture2D("
        "           texture,"
        "           out_texturePosition"
        "   ).a);"
        "}");
    return textShader;
}
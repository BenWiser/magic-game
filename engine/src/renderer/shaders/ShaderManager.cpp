#include "renderer/shaders/ShaderManager.h"

namespace ShaderManager
{
using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

typedef int ShaderID;

ShaderID activeShader = -1;

vector<shared_ptr<Shader>> shaderManager;

ShaderID create(string vertexSource, string fragmentSource)
{
    shaderManager.push_back(make_shared<Shader>(vertexSource, fragmentSource));

    return shaderManager.size() - 1;
}

shared_ptr<Shader> getShader(const ShaderID &shaderId)
{
    if (shaderId < 0 || shaderId >= shaderManager.size())
    {
        throw "Invalid shader ID provided";
    }

    return shaderManager[shaderId];
}

void setActiveShader(const ShaderID &shaderId)
{
    activeShader = shaderId;
}

shared_ptr<Shader> getActiveShader()
{
    if (activeShader < 0 || activeShader > shaderManager.size())
    {
        return nullptr;
    }

    return shaderManager[activeShader];
}
} // namespace ShaderManager
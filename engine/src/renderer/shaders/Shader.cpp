#include <vector>
#include <iostream>

#include "renderer/shaders/Shader.h"

Shader::Shader(const std::string &vertexSource, const std::string &FragmentSource)
{
    _programId = glCreateProgram();
    auto vertexShaderId = compileShader(GL_VERTEX_SHADER, vertexSource);
    auto fragmentShaderId = compileShader(GL_FRAGMENT_SHADER, FragmentSource);
    glAttachShader(_programId, vertexShaderId);
    glAttachShader(_programId, fragmentShaderId);
    glLinkProgram(_programId);
}

Shader::~Shader()
{
    glDeleteProgram(_programId);
}

GLuint Shader::compileShader(GLuint shaderType, const std::string &source)
{
    using std::string;

    auto vertexShaderPointer = source.c_str();
    GLint lengths{static_cast<int>(source.size())};

    auto shaderId = glCreateShader(shaderType);

    glShaderSource(shaderId, 1, &vertexShaderPointer, &lengths);
    glCompileShader(shaderId);

    GLsizei length = 100;
    std::vector<GLchar> infoLog(length);
    glGetShaderInfoLog(shaderId, length, &length, &infoLog[0]);

    std::cout << infoLog.data() << std::endl;

    return shaderId;
}

unsigned int Shader::getAttribLocation(const std::string &attributeName)
{
    auto location = glGetAttribLocation(_programId, attributeName.c_str());
    return location;
}

unsigned int Shader::getUniform(const std::string &uniformName)
{
    auto location = glGetUniformLocation(_programId, uniformName.c_str());
    return location;
}

void Shader::useShader() const
{
    glUseProgram(_programId);
}

void Shader::disableShader() const
{
    glUseProgram(0);
}
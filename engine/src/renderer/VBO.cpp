
#include "openGL.h"
#include "renderer/VBO.h"

VBO::VBO(std::vector<float> vertexs, std::vector<float> texCoords)
{
    glGenBuffers(1, &_vertexBufferId);
    glGenBuffers(1, &_textureBufferId);

    _vertexCount = vertexs.size() / 2;

    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexs.size(), &vertexs[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, _textureBufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
}

VBO::~VBO()
{
    glDeleteBuffers(1, &_vertexBufferId);
    glDeleteBuffers(1, &_textureBufferId);
}

void VBO::render(
    std::shared_ptr<Shader> shader,
    const float &frame,
    const float &animation,
    const float &uWidth,
    const float &vHeight,
    const float &alpha)
{
    const auto vertexLocation = shader->getAttribLocation("vertexPosition");
    const auto textureLocation = shader->getAttribLocation("texturePosition");
    const auto uvDimenionsUniform = shader->getUniform("uvDimenions");
    const auto animationInformationUniform = shader->getUniform("animationInformation");
    const auto alphaUniform = shader->getUniform("alpha");

    glUniform2f(uvDimenionsUniform, uWidth, vHeight);
    glUniform2f(animationInformationUniform, frame, animation);
    glUniform1f(alphaUniform, alpha);

    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferId);
    glEnableVertexAttribArray(vertexLocation);
    glVertexAttribPointer(vertexLocation, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);

    glBindBuffer(GL_ARRAY_BUFFER, _textureBufferId);
    glEnableVertexAttribArray(textureLocation);
    glVertexAttribPointer(textureLocation, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, _vertexCount);

    glEnableVertexAttribArray(textureLocation);
    glDisableVertexAttribArray(vertexLocation);
}
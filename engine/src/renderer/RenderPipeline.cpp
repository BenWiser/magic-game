#include "renderer/RenderPipeline.h"
#include "renderer/shaders/ShaderManager.h"

namespace RenderPipeline
{
using std::make_shared;
using std::shared_ptr;

void PipeLine::enque(RenderCommand command)
{
    _renderPipeline.push(command);
}

RenderCommand PipeLine::dequeue()
{
    auto renderCommand = _renderPipeline.front();
    _renderPipeline.pop();
    return renderCommand;
}

bool PipeLine::hasNext()
{
    return !_renderPipeline.empty();
}

void PipeLine::render(Renderer &renderer)
{
    while (hasNext())
    {
        auto command = dequeue();

        if (command.type == "rect")
        {
            renderer.renderRect(
                command.textureId,
                command.x,
                command.y,
                command.width,
                command.height,
                command.angle);
        }
        if (command.type == "rect_colour")
        {
            renderer.renderRectColour(
                command.r,
                command.g,
                command.b,
                command.x,
                command.y,
                command.width,
                command.height,
                command.angle);
        }
        else if (command.type == "spritemap")
        {
            renderer.renderSpriteMap(
                command.textureId,
                command.maxFrames,
                command.animations,
                command.frame,
                command.animation,
                command.x,
                command.y,
                command.width,
                command.height,
                command.angle);
        }
        else if (command.type == "alpha")
        {
            renderer.setAlpha(command.alpha);
        }
        else if (command.type == "depth")
        {
            renderer.setDepth(command.depth);
        }
        else if (command.type == "tiling")
        {
            renderer.setTiling(command.x, command.y);
        }
        else if (command.type == "text")
        {
            renderer.renderText(command.text, command.fontPath, command.x, command.y, command.width);
        }
        else if (command.type == "shader")
        {
            ShaderManager::setActiveShader(command.shaderId);
        }
    }
}

RenderCommand PipeLine::createRect(
    unsigned int textureId,
    float x,
    float y,
    float width,
    float height,
    float angle) const
{
    RenderCommand command;
    command.type = "rect";
    command.textureId = textureId;
    command.x = x;
    command.y = y;
    command.width = width;
    command.height = height;
    command.angle = angle;

    return command;
}

RenderCommand PipeLine::createRectColour(
        float r,
        float g,
        float b,
        float x,
        float y,
        float width,
        float height,
        float angle) const
{
    RenderCommand command;
    command.type = "rect_colour";
    command.r = r;
    command.g = g;
    command.b = b;
    command.x = x;
    command.y = y;
    command.width = width;
    command.height = height;
    command.angle = angle;

    return command;
}

RenderCommand PipeLine::createSpriteMap(
    unsigned int textureId,
    float maxFrames,
    float animations,
    float frame,
    float animation,
    float x,
    float y,
    float width,
    float height,
    float angle) const
{
    RenderCommand command;
    command.type = "spritemap";
    command.textureId = textureId;
    command.maxFrames = maxFrames;
    command.animations = animations;
    command.frame = frame;
    command.animation = animation;
    command.x = x;
    command.y = y;
    command.width = width;
    command.height = height;
    command.angle = angle;

    return command;
}

RenderCommand PipeLine::createText(
    std::string text,
    std::string fontPath,
    float x,
    float y,
    float size)
{
    RenderCommand command;
    command.type = "text";
    command.fontPath = fontPath,
    command.text = text;
    command.x = x;
    command.y = y;
    command.width = size;
    command.height = size;
    return command;
}

RenderCommand PipeLine::setAlpha(
    const float &alpha)
{
    RenderCommand command;
    command.type = "alpha";
    command.alpha = alpha;

    return command;
}

RenderCommand PipeLine::setDepth(
    const float &depth)
{
    RenderCommand command;
    command.type = "depth";
    command.depth = depth;

    return command;
}

RenderCommand PipeLine::setTiling(
    const float &horizontalTiling,
    const float &verticalTiling)
{
    RenderCommand command;
    command.type = "tiling";
    command.x = horizontalTiling;
    command.y = verticalTiling;

    return command;
}

RenderCommand PipeLine::setShader(
    int shaderId)
{
    RenderCommand command;
    command.type = "shader";
    command.shaderId = shaderId;

    return command;
}

shared_ptr<PipeLine> getPipeLine()
{
    static auto pipeline = make_shared<PipeLine>();
    return pipeline;
}
} // namespace RenderPipeline
#include <sstream>

#include "Configuration.h"
#include "FileReader.h"

Configuration::Configuration()
{
    auto manifest = FileReader::readFile("./manifest");
    if (manifest == "")
    {
        _game_name = "Game";
    }
    
    std::stringstream manifest_stream(manifest);
    std::string line;

    while(std::getline(manifest_stream, line, '\n'))
    {
        std::stringstream value_stream(line);
        std::string datum;

        std::string key = "";

        for (int i = 0; std::getline(value_stream, datum, '=') && i < 2; i++)
        {
            if (i == 0)
            {
                key = datum;
            }
            else
            {
                if (key == "GAME_NAME")
                {
                    _game_name = datum;
                }
            }
        }
    }
}

std::string Configuration::getGameName()
{
    return _game_name;
}
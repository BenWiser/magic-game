
#include "sound/SoundManager.h"

namespace SoundManager
{
Manager::Manager()
{
    _device = alcOpenDevice(NULL);

    _context = alcCreateContext(_device, NULL);
    alcMakeContextCurrent(_context);

    alGenSources(1, &_source);

    alSourcef(_source, AL_PITCH, 1);
    alSourcef(_source, AL_GAIN, 1);
    alSource3f(_source, AL_POSITION, 0, 0, 0);
    alSource3f(_source, AL_VELOCITY, 0, 0, 0);
    alSourcei(_source, AL_LOOPING, AL_FALSE);
}

void Manager::shutdown()
{
    alSourceStop(_source);

    alcMakeContextCurrent(NULL);
    alDeleteSources(1, &_source);

    delete _context;
    delete _device;
}

void Manager::stopSound()
{
    alSourceStop(_source);
}

void Manager::playSound(const Sound &sound)
{
    alSourcei(_source, AL_BUFFER, sound.bufferId);
    alSourcePlay(_source);
}

std::shared_ptr<Manager> getSoundManager()
{
    static std::shared_ptr<Manager> manager{new Manager()};
    return manager;
}
} // namespace SoundManager
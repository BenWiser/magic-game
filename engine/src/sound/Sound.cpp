#include <fstream>
#include <vector>

#include "sound/Sound.h"

#include <iostream>

// The wav header spec, taken from https://i.stack.imgur.com/ITplE.gif
typedef struct
{
    uint8_t RIFF[4];
    uint32_t chunkSize;
    uint8_t WAVE[4];

    uint8_t format[4];
    uint32_t subchunk1Size;
    uint16_t sudioFormat;
    uint16_t numOfChan;
    uint32_t samplesPerSec;
    uint32_t bytesPerSec;
    uint16_t blockAlign;
    uint16_t bitsPerSample;

    uint8_t subchunk2ID[4];
    uint32_t subchunk2Size;
} WavHeader;

Sound::Sound(std::string soundFile)
{
    ALsizei size, freq;
    ALenum format;
    ALvoid *data;
    ALboolean loop = AL_FALSE;

    auto wavFile = fopen(soundFile.c_str(), "r");

    WavHeader header;
    fread(&header, 1, sizeof(WavHeader), wavFile);

    auto bytesPerSample = header.bitsPerSample / 8;
    auto samples = header.chunkSize / bytesPerSample;

    std::vector<char> bufferData;

    char buffer;
    while (fread(&buffer, 1, sizeof(char), wavFile) > 0)
    {
        bufferData.push_back(buffer);
    }

    alGenBuffers(1, &bufferId);

    alBufferData(bufferId, AL_FORMAT_MONO16, &bufferData[0], bufferData.size(), header.samplesPerSec);
}

Sound::~Sound()
{
    alDeleteBuffers(1, &bufferId);
}
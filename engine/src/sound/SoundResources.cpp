
#include "sound/SoundResources.h"

#include <iostream>

namespace SoundResources
{
using std::shared_ptr;
using std::string;
using std::vector;

vector<shared_ptr<Sound>> sounds;

soundId loadSound(string fileName)
{
    const auto sound = std::make_shared<Sound>(fileName);
    sounds.push_back(sound);

    return sounds.size() - 1;
}

shared_ptr<Sound> getSound(soundId sound)
{
    return sounds[sound];
}
} // namespace SoundResources
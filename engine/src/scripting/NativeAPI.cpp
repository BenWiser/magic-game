#include <iostream>

#include "GameState.h"
#include "FileWriter.h"
#include "FileReader.h"
#include "Vector2.h"
#include "sound/SoundManager.h"
#include "sound/SoundResources.h"
#include "scripting/NativeAPI.h"
#include "physics/PhysicsEngine.h"
#include "renderer/shaders/ShaderManager.h"
#include "renderer/RenderPipeline.h"
#include "PNGLoader.h"

int create_texture(lua_State *state)
{
    auto fileName = std::string(lua_tostring(state, 1));

    unsigned int textureId;
    PNGLoader::loadPNG(fileName, textureId);

    lua_pushinteger(state, textureId);

    return 1;
}

int destroy_texture(lua_State *state)
{
    auto textureId = lua_tointeger(state, 1);

    PNGLoader::unloadTexture(textureId);

    return 0;
}

int render_rect_colour(lua_State *state)
{
    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->createRectColour(
        lua_tonumber(state, 1),
        lua_tonumber(state, 2),
        lua_tonumber(state, 3),
        lua_tonumber(state, 4),
        lua_tonumber(state, 5),
        lua_tonumber(state, 6),
        lua_tonumber(state, 7),
        lua_tonumber(state, 8));
    renderPipeline->enque(command);

    return 0;
}

int render_rect(lua_State *state)
{
    using RenderPipeline::PipeLine;

    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->createRect(
        lua_tointeger(state, 1),
        lua_tonumber(state, 2),
        lua_tonumber(state, 3),
        lua_tonumber(state, 4),
        lua_tonumber(state, 5),
        lua_tonumber(state, 6));
    renderPipeline->enque(command);

    return 0;
}

int render_spritemap_frame(lua_State *state)
{
    using RenderPipeline::PipeLine;

    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->createSpriteMap(
        lua_tointeger(state, 1),
        lua_tonumber(state, 2),
        lua_tonumber(state, 3),
        lua_tonumber(state, 4),
        lua_tonumber(state, 5),
        lua_tonumber(state, 6),
        lua_tonumber(state, 7),
        lua_tonumber(state, 8),
        lua_tonumber(state, 9),
        lua_tonumber(state, 10));
    renderPipeline->enque(command);

    return 0;
}

int render_text(lua_State *state)
{
    using RenderPipeline::PipeLine;

    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->createText(
        lua_tostring(state, 1),
        lua_tostring(state, 2),
        lua_tonumber(state, 3),
        lua_tonumber(state, 4),
        lua_tonumber(state, 5));
    renderPipeline->enque(command);

    return 0;
}

int render_set_alpha(lua_State *state)
{
    using RenderPipeline::PipeLine;

    float number = lua_tonumber(state, 1);
    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->setAlpha(
        number);
    renderPipeline->enque(command);

    return 0;
}

int render_set_depth(lua_State *state)
{
    using RenderPipeline::PipeLine;

    float number = lua_tonumber(state, 1);
    auto renderPipeline = RenderPipeline::getPipeLine();
    auto command = renderPipeline->setDepth(
        number);
    renderPipeline->enque(command);

    return 0;
}

int render_set_tiling(lua_State *state)
{
    using RenderPipeline::PipeLine;
    const auto horizontalTiling = lua_tonumber(state, 1);
    const auto verticalTiling = lua_tonumber(state, 2);

    const auto renderPipeline = RenderPipeline::getPipeLine();
    const auto command = renderPipeline->setTiling(
        horizontalTiling,
        verticalTiling);
    renderPipeline->enque(command);

    return 0;
}

int render_shader_create(lua_State *state)
{
    const auto shaderId = ShaderManager::create(
        lua_tostring(state, 1),
        lua_tostring(state, 2));

    lua_pushinteger(state, shaderId);

    return 1;
}

int render_shader_set(lua_State *state)
{
    using RenderPipeline::PipeLine;
    const auto shaderId = lua_tonumber(state, 1);

    const auto renderPipeline = RenderPipeline::getPipeLine();
    const auto command = renderPipeline->setShader(shaderId);
    renderPipeline->enque(command);

    return 0;
}

int render_shader_set_default(lua_State *state)
{
    using RenderPipeline::PipeLine;

    const auto renderPipeline = RenderPipeline::getPipeLine();
    const auto command = renderPipeline->setShader(-1);
    renderPipeline->enque(command);

    return 0;
}

int iterate_physics(lua_State* state)
{
    PhysicsEngine::iterate();
    return 0;
}

int set_physics_gravity(lua_State* state)
{
    Vector2 force {
        lua_tonumber(state, 1),
        lua_tonumber(state, 2)
    };
    PhysicsEngine::setGravity(force);
    return 0;
}

int create_physics_body(lua_State *state)
{
    auto x = lua_tonumber(state, 1);
    auto y = lua_tonumber(state, 2);
    auto width = lua_tonumber(state, 3);
    auto height = lua_tonumber(state, 4);
    auto friction = lua_tonumber(state, 5);
    auto density = lua_tonumber(state, 6);
    auto isDynamic = lua_toboolean(state, 7);
    auto fixedRotation = lua_toboolean(state, 8);
    auto isSensor = lua_toboolean(state, 9);

    int bodyId;

    PhysicsEngine::createBody(
        Vector2(x, y),
        Vector2(width, height),
        isDynamic,
        fixedRotation,
        friction,
        density,
        isSensor,
        bodyId);

    lua_pushinteger(state, bodyId);

    return 1;
}

int apply_force_to_physics_body(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto forceX = lua_tonumber(state, 2);
    auto forceY = lua_tonumber(state, 3);

    PhysicsEngine::applyForce(Vector2(forceX, forceY), bodyId);

    return 0;
}

int apply_impulse_to_physics_body(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto forceX = lua_tonumber(state, 2);
    auto forceY = lua_tonumber(state, 3);

    PhysicsEngine::applyImpulse(Vector2(forceX, forceY), bodyId);

    return 0;
}

int apply_angular_impulse_to_physics_body(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto angle = lua_tonumber(state, 2);

    PhysicsEngine::applyAngularImpulse(angle, bodyId);

    return 0;
}

int destroy_physics_body(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);

    PhysicsEngine::destroyBody(bodyId);

    return 0;
}

int get_physics_body_position(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);

    auto body = PhysicsEngine::bodies[bodyId];
    auto bodyPosition = body->GetPosition();
    auto angle = body->GetAngle();

    lua_pushnumber(state, bodyPosition.x);
    lua_pushnumber(state, bodyPosition.y);
    lua_pushnumber(state, angle);

    return 3;
}

int set_physics_body_position(lua_State* state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto x = lua_tonumber(state, 2);
    auto y = lua_tonumber(state, 3);
    auto rotation = lua_tonumber(state, 4);
    
    auto body = PhysicsEngine::bodies[bodyId];
    body->SetTransform(b2Vec2(x, y), rotation);

    return 0;
}

int set_physics_body_velocity(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto forceX = lua_tonumber(state, 2);
    auto forceY = lua_tonumber(state, 3);
    auto body = PhysicsEngine::bodies[bodyId];

    body->SetLinearVelocity(b2Vec2(forceX, forceY));

    return 0;
}

int get_physics_body_velocity(lua_State *state)
{
    auto bodyId = lua_tointeger(state, 1);
    auto body = PhysicsEngine::bodies[bodyId];
    auto bodyLinearVelocity = body->GetLinearVelocity();

    lua_pushnumber(state, bodyLinearVelocity.x);
    lua_pushnumber(state, bodyLinearVelocity.y);

    return 2;
}

int physics_has_collided(lua_State *state)
{
    auto bodyA = lua_tointeger(state, 1);
    auto bodyB = lua_tointeger(state, 2);

    auto hasCollided = PhysicsEngine::hasCollided(bodyA, bodyB);

    lua_pushboolean(state, hasCollided);

    return 1;
}

int physics_query_collisions(lua_State *state)
{
    auto body = lua_tointeger(state, 1);

    auto collisions = PhysicsEngine::queryCollisions(body);

    lua_newtable(state);
    auto iterator = collisions.begin();
    for (unsigned int i = 1; iterator != collisions.end(); i++, iterator++)
    {
        lua_pushinteger(state, i);
        lua_pushinteger(state, *iterator);
        lua_rawset(state, -3);
    }

    return 1;
}

int is_key_pressed(lua_State *state)
{
    auto keyCode = lua_tointeger(state, 1);

    bool isPressed;
    if (keyCode >= 0 && keyCode < 350)
    {
        isPressed = GameState::keyState[keyCode];
    }

    lua_pushboolean(state, isPressed);

    return 1;
}

int is_mouse_button_pressed(lua_State *state)
{
    auto keyCode = lua_tointeger(state, 1);

    bool isPressed;
    if (keyCode == 0 || keyCode == 1)
    {
        isPressed = GameState::mouseState[keyCode];
    }

    lua_pushboolean(state, isPressed);

    return 1;
}

int get_mouse_position(lua_State *state)
{
    auto mousePositionX = GameState::mousePosition.x;
    auto mousePositionY = GameState::mousePosition.y;

    // The camera gets moved around so the position that the player is clicking
    // might not be the actual coordinates
    // Invert the camera
    mousePositionX -= GameState::VIEWPORT_OFFSET_X;
    mousePositionY -= GameState::VIEWPORT_OFFSET_Y;

    mousePositionX /= GameState::VIEWPORT_WIDTH / GameState::WIDTH;
    mousePositionY /= GameState::VIEWPORT_HEIGHT / GameState::HEIGHT;

    mousePositionX -= GameState::HALF_WIDTH;
    mousePositionY -= GameState::HALF_HEIGHT;

    mousePositionX /= GameState::cameraZoom;
    mousePositionY /= GameState::cameraZoom;

    mousePositionX += GameState::cameraPosition.x;
    mousePositionY += GameState::cameraPosition.y;

    lua_pushnumber(state, mousePositionX);
    lua_pushnumber(state, mousePositionY);

    return 2;
}

int get_mouse_scroll(lua_State *state)
{
    lua_pushnumber(state, GameState::mouseScroll);

    return 1;
}

int ray_trace(lua_State *state)
{
    auto x1 = lua_tonumber(state, 1);
    auto y1 = lua_tonumber(state, 2);
    auto x2 = lua_tonumber(state, 3);
    auto y2 = lua_tonumber(state, 4);

    auto bodies = PhysicsEngine::rayTrace(Vector2(x1, y1), Vector2(x2, y2));

    lua_newtable(state);
    for (unsigned int i = 0; i < bodies.size(); i++)
    {
        lua_pushinteger(state, i + 1);
        lua_pushinteger(state, bodies[i]);
        lua_rawset(state, -3);
    }

    return 1;
}

int query_bounds(lua_State *state)
{
    auto x1 = lua_tonumber(state, 1);
    auto y1 = lua_tonumber(state, 2);
    auto x2 = lua_tonumber(state, 3);
    auto y2 = lua_tonumber(state, 4);

    auto bodies = PhysicsEngine::queryBounds(Vector2(x1, y1), Vector2(x2, y2));

    lua_newtable(state);
    for (unsigned int i = 0; i < bodies.size(); i++)
    {
        lua_pushinteger(state, i + 1);
        lua_pushinteger(state, bodies[i]);
        lua_rawset(state, -3);
    }

    return 1;
}

int get_screen_dimensions(lua_State *state)
{
    lua_pushnumber(state, GameState::WIDTH);
    lua_pushnumber(state, GameState::HEIGHT);

    return 2;
}

int move_camera(lua_State *state)
{
    GameState::cameraPosition.x = lua_tonumber(state, 1);
    GameState::cameraPosition.y = lua_tonumber(state, 2);
    GameState::cameraZoom = lua_tonumber(state, 3);

    return 0;
}

int get_camera(lua_State *state)
{
    lua_pushnumber(state, GameState::cameraPosition.x);
    lua_pushnumber(state, GameState::cameraPosition.y);
    lua_pushnumber(state, GameState::cameraZoom);

    return 3;
}

int file_write(lua_State *state)
{
    const auto fileName = lua_tostring(state, 1);
    const auto output = lua_tostring(state, 2);

    FileWriter::writeFile(fileName, output);

    return 0;
}

int file_read(lua_State *state)
{
    const auto fileName = lua_tostring(state, 1);

    const auto output = FileReader::readFile(fileName);

    lua_pushstring(state, output.c_str());

    return 1;
}

int sound_load(lua_State *state)
{
    const auto fileName = lua_tostring(state, 1);

    const auto soundId = SoundResources::loadSound(fileName);

    lua_pushinteger(state, soundId);

    return 1;
}

int sound_play(lua_State *state)
{
    const auto soundManager = SoundManager::getSoundManager();
    const auto soundId = lua_tointeger(state, 1);

    const auto sound = SoundResources::getSound(soundId);

    soundManager->playSound(*sound);

    return 1;
}

int print(lua_State* state)
{
    auto str = lua_tostring(state, 1);
    std::cout << str << std::endl;
    return 0;
}
#include <iostream>

#include "ErrorCodes.h"
#include "scripting/ScriptEngine.h"
#include "scripting/NativeAPI.h"

void ScriptEngine::init()
{
    _state = luaL_newstate();

    // Make the standard library available
    luaL_openlibs(_state);

    addAPI("Magic_create_texture", create_texture);
    addAPI("Magic_destroy_texture", destroy_texture);
    addAPI("Magic_render_rect_colour", render_rect_colour);
    addAPI("Magic_render_rect", render_rect);
    addAPI("Magic_render_spritemap_frame", render_spritemap_frame);
    addAPI("Magic_render_text", render_text);
    addAPI("Magic_render_set_alpha", render_set_alpha);
    addAPI("Magic_render_set_depth", render_set_depth);
    addAPI("Magic_render_set_tiling", render_set_tiling);
    addAPI("Magic_render_shader_create", render_shader_create);
    addAPI("Magic_render_shader_set", render_shader_set);
    addAPI("Magic_render_shader_set_default", render_shader_set_default);
    addAPI("Magic_iterate_physics", iterate_physics);
    addAPI("Magic_set_physics_gravity", set_physics_gravity);
    addAPI("Magic_create_physics_body", create_physics_body);
    addAPI("Magic_apply_force_to_physics_body", apply_force_to_physics_body);
    addAPI("Magic_apply_impulse_to_physics_body", apply_impulse_to_physics_body);
    addAPI("Magic_apply_angular_impulse_to_physics_body", apply_angular_impulse_to_physics_body);
    addAPI("Magic_destroy_physics_body", destroy_physics_body);
    addAPI("Magic_get_physics_body_position", get_physics_body_position);
    addAPI("Magic_set_physics_body_position", set_physics_body_position);
    addAPI("Magic_get_physics_body_velocity", get_physics_body_velocity);
    addAPI("Magic_set_physics_body_velocity", set_physics_body_velocity);
    addAPI("Magic_physics_has_collided", physics_has_collided);
    addAPI("Magic_physics_query_collisions", physics_query_collisions);
    addAPI("Magic_is_key_pressed", is_key_pressed);
    addAPI("Magic_is_mouse_button_pressed", is_mouse_button_pressed);
    addAPI("Magic_get_mouse_position", get_mouse_position);
    addAPI("Magic_get_mouse_scroll", get_mouse_scroll);
    addAPI("Magic_ray_trace", ray_trace);
    addAPI("Magic_query_bounds", query_bounds);
    addAPI("Magic_get_screen_dimensions", get_screen_dimensions);
    addAPI("Magic_move_camera", move_camera);
    addAPI("Magic_get_camera", get_camera);
    addAPI("Magic_file_write", file_write);
    addAPI("Magic_file_read", file_read);
    addAPI("Magic_sound_load", sound_load);
    addAPI("Magic_sound_play", sound_play);
    addAPI("Magic_print", print);
}

int ScriptEngine::loadBytecode(const std::string &fileName) const
{
    using std::cout;
    using std::endl;

    int result = luaL_dofile(_state, fileName.c_str());

    if (result != LUA_OK)
    {
        auto error = lua_tostring(_state, -1);
        cout << "Failed to load bytecode " << error << endl;
        return ErrorCodes::FAILED_TO_LOAD_SCRIPT;
    }

    return ErrorCodes::SUCCESS;
}

int ScriptEngine::loadScript(const std::string &script) const
{
    using std::cout;
    using std::endl;

    int result = luaL_loadstring(_state, script.c_str());

    if (result != LUA_OK)
    {
        auto error = lua_tostring(_state, -1);
        cout << "Failed to load script " << error << endl;
        return ErrorCodes::FAILED_TO_LOAD_SCRIPT;
    }

    return ErrorCodes::SUCCESS;
}

int ScriptEngine::addAPI(const std::string &name, int (*scriptFunction)(lua_State* state)) const
{
    lua_register(_state, name.c_str(), scriptFunction);
}

int ScriptEngine::loadLibrary(const std::string &name, const std::string &script) const
{
    lua_getglobal(_state, "package");
    lua_getfield(_state, -1, "preload");
    loadScript(script);
    lua_setfield(_state, -2, name.c_str());
    return ErrorCodes::SUCCESS;
}

/**
 * The script engine in use is Lua to make it easier to
 * quickly iterate and modify the game logic
 */
int ScriptEngine::execute()
{
    int result = lua_pcall(_state, 0, LUA_MULTRET, 0);

    if (result != LUA_OK)
    {
        return result;
    }

    return LUA_OK;
}

void ScriptEngine::gameLoop()
{
    auto result = lua_getglobal(_state, "gameLoop");
    if (result != NULL)
    {
        lua_call(_state, 0, 0);
    }
    else
    {
        lua_pop(_state, 1);
    }
}

void ScriptEngine::cleanUp()
{
    auto result = lua_getglobal(_state, "cleanUp");
    if (result != NULL)
    {
        lua_call(_state, 0, 0);
    }
    else
    {
        lua_pop(_state, 1);
    }
    lua_close(_state);
    _state = nullptr;
}

ScriptEngine::~ScriptEngine()
{
    if (_state != nullptr)
    {
        lua_close(_state);
    }
}
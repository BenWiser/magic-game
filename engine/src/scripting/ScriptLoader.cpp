#include <fstream>
#include <vector>
#include <filesystem>
#include <algorithm>

#include "FileReader.h"

#include "scripting/ScriptLoader.h"

namespace ScriptLoader
{
namespace fs = std::filesystem;

void loadScript(
    const ScriptEngine &scriptEngine,
    const string &mainFileName)
{
    auto mainScript = FileReader::readFile(mainFileName);

    scriptEngine.loadScript(mainScript);
}

void loadLibraries(const ScriptEngine &scriptEngine, const string &directory)
{
    for (const auto &entry : fs::recursive_directory_iterator(directory))
    {
        if (!entry.is_regular_file())
        {
            continue;
        }
        string path = entry.path().string();
        string script = FileReader::readFile(path);
        // Get rid of the directory to load libraries from and also remove the ".lua"
        string name = path.substr(
            directory.size() + 1,
            path.size() - directory.size() - 5
        );
        // Replace "/" with "." to follow the standard lua require syntax
        std::replace(name.begin(), name.end(), '/', '.');

        scriptEngine.loadLibrary(name, script);
    }
}
} // namespace ScriptLoader
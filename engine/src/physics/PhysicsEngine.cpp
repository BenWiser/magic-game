
#include "physics/PhysicsEngine.h"
#include "GameState.h"

namespace PhysicsEngine
{
using std::shared_ptr;
using std::vector;

class RayCastCallback : public b2RayCastCallback
{
private:
    vector<int> _bodies;

public:
    float32 ReportFixture(
        b2Fixture *fixture,
        const b2Vec2 &point,
        const b2Vec2 &normal,
        float32 fraction)
    {
        auto bodyId = static_cast<int *>(fixture->GetUserData());
        _bodies.push_back(*bodyId);
    }

    vector<int> getCollidedBodies() const
    {
        return _bodies;
    }
};

class QueryCallback: public b2QueryCallback
{
private:
    vector<int> _bodies;

public:
    bool ReportFixture(b2Fixture* fixture)
    {
        auto bodyId = static_cast<int *>(fixture->GetUserData());
        _bodies.push_back(*bodyId);
    }

    vector<int> getCollidedBodies() const
    {
        return _bodies;
    }
};

map<int, b2Body *> bodies;

shared_ptr<b2World> createWorld()
{
    static shared_ptr<b2World> world{new b2World(b2Vec2(0.0f, 0.0f))};
    return world;
}

void iterate()
{
    constexpr int32 velocityIterations = 6;
    constexpr int32 positionIterations = 2;
    auto world = createWorld();

    // TODO: Nasty hack to make the physics engine feel faster
    for (int i = 0; i < 15; i++)
    {
        world->Step(0.01, velocityIterations, positionIterations);
    }
}

b2Body *createBody(
    const Vector2 &position,
    const Vector2 &dimensions,
    const bool &isDynamic,
    const bool &fixedRotation,
    const float& friction,
    const float &density,
    const bool &isSensor,
    int &bodyId)
{
    static int maxId = 0;
    maxId++;
    bodyId = maxId;

    auto world = createWorld();

    b2BodyDef bodyDef;
    bodyDef.position.Set(position.x, position.y);
    bodyDef.fixedRotation = fixedRotation;
    bodyDef.linearDamping = 0.05f;
    if (isDynamic)
    {
        bodyDef.type = b2_dynamicBody;
    }
    else
    {
        bodyDef.type = b2_kinematicBody;
    }

    auto body = world->CreateBody(&bodyDef);

    b2PolygonShape bodyShape;
    bodyShape.SetAsBox(dimensions.x / 2, dimensions.y / 2);

    b2FixtureDef fixtureDef;
    fixtureDef.isSensor = isSensor;
    fixtureDef.density = density;
    fixtureDef.friction = friction;
    fixtureDef.shape = &bodyShape;
    fixtureDef.userData = new int{bodyId};

    body->CreateFixture(&fixtureDef);

    bodies[bodyId] = body;

    return body;
}

void setGravity(const Vector2 &force)
{
    auto world = createWorld();
    world->SetGravity(b2Vec2(force.x, force.y));
}

void applyForce(const Vector2 &force, const int &bodyId)
{
    auto world = createWorld();
    auto body = bodies[bodyId];
    body->ApplyForceToCenter(b2Vec2(body->GetMass() * force.x, body->GetMass() * force.y), true);
}

void applyImpulse(const Vector2 &force, const int &bodyId)
{
    auto world = createWorld();
    auto body = bodies[bodyId];
    body->ApplyLinearImpulseToCenter(b2Vec2(body->GetMass() * force.x, body->GetMass() * force.y), true);
}

void applyAngularImpulse(const float &angle, const int &bodyId)
{
    const auto world = createWorld();
    const auto body = bodies[bodyId];
    body->ApplyAngularImpulse(angle, true);
}

std::vector<int> rayTrace(const Vector2 &pointA, const Vector2 &pointB)
{
    auto world = createWorld();
    RayCastCallback raycastCallback;
    world->RayCast(&raycastCallback, b2Vec2(pointA.x, pointA.y), b2Vec2(pointB.x, pointB.y));

    auto collisions = raycastCallback.getCollidedBodies();
    return collisions;
}

std::vector<int> queryBounds(const Vector2 &pointA, const Vector2 &pointB)
{
    auto world = createWorld();

    QueryCallback queryCallback;
    b2AABB bounds;
    bounds.upperBound = b2Vec2(pointA.x, pointA.y);
    bounds.lowerBound = b2Vec2(pointB.x, pointB.y);
    
    world->QueryAABB(&queryCallback, bounds);

    auto collisions = queryCallback.getCollidedBodies();
    return collisions;
}

std::set<int> queryCollisions(const int &bodyId)
{
    std::set<int> collisions {};

    auto world = createWorld();
    auto contact = bodies[bodyId]->GetContactList();

    while (contact != nullptr)
    {
        auto fixtureAID = static_cast<int *>(contact->contact->GetFixtureA()->GetUserData());
        auto fixtureBID = static_cast<int *>(contact->contact->GetFixtureB()->GetUserData());

        if (*fixtureAID != bodyId)
        {
            collisions.insert(*fixtureAID);
        }

        if (*fixtureBID != bodyId)
        {
            collisions.insert(*fixtureBID);
        }

        contact = contact->next;
    }

    return collisions;
}

bool hasCollided(const int &bodyIdA, const int &bodyIdB)
{
    auto world = createWorld();

    auto contact = bodies[bodyIdA]->GetContactList();

    while (contact != nullptr)
    {
        auto fixtureAID = static_cast<int *>(contact->contact->GetFixtureA()->GetUserData());
        auto fixtureBID = static_cast<int *>(contact->contact->GetFixtureB()->GetUserData());

        // The body A and B could be either fixture so check against both
        if (*fixtureAID == bodyIdB || *fixtureBID == bodyIdB)
        {
            return true;
        }

        contact = contact->next;
    }

    return false;
}

void destroyBody(const int &bodyId)
{
    auto world = createWorld();
    auto body = bodies[bodyId];
    delete body->GetUserData();
    world->DestroyBody(body);
    bodies.erase(bodyId);
}
} // namespace PhysicsEngine
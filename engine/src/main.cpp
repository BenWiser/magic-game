#include <chrono>
#include <thread>
#include <string>

#include "GameState.h"
#include "ErrorCodes.h"
#include "sound/SoundManager.h"
#include "scripting/ScriptLoader.h"
#include "scripting/ScriptEngine.h"
#include "editor/Editor.h"
#include "renderer/Renderer.h"
#include "renderer/RenderPipeline.h"
#include "physics/PhysicsEngine.h"

int main(int argc, char **argv)
{
    using std::string;
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    using std::this_thread::sleep_for;

    GameState::init();

    const auto soundManager = SoundManager::getSoundManager();

    Renderer renderer;
    renderer.initialise();

    std::string editor_asset_directory = "";

    if (argc > 2)
    {
        editor_asset_directory = argv[2];
    }

    Editor editor(editor_asset_directory);

    const std::string script_dir = "./assets/scripts";

    string gameScript = script_dir + "/main.lua";

    if (editor.isEnabled() && argc > 1)
    {
        gameScript = argv[1];
    }

    auto renderPipeline = RenderPipeline::getPipeLine();

    ScriptEngine scriptEngine;
    auto loadScriptEngine = [&scriptEngine, &script_dir, &gameScript, &editor, &editor_asset_directory]() {
        scriptEngine.init();
        editor.registerAPIs(scriptEngine);
        scriptEngine.loadBytecode("./assets/lib");
        ScriptLoader::loadLibraries(scriptEngine, script_dir);
        if (editor.isEnabled() && !editor_asset_directory.empty())
        {
            ScriptLoader::loadLibraries(scriptEngine, editor_asset_directory + "/scripts");
        }
        ScriptLoader::loadScript(scriptEngine, gameScript);
        scriptEngine.execute();
    };

    loadScriptEngine();

    editor.addMenuButton("Restart", [&scriptEngine, &loadScriptEngine]() {
        scriptEngine.cleanUp();
        loadScriptEngine();
    });

    // Time between frames
    constexpr float32 timeStep = 1.0f / GameState::MAX_FRAMERATE;

    // This is the total amount of time that the frames should sleep for between each loop
    const auto timeToSleep = milliseconds((int)(timeStep * 1000));

    auto sleepFor = timeToSleep;
    renderer.gameLoop([&sleepFor, &scriptEngine, &renderPipeline, &renderer, &timeToSleep, &editor]() {
        auto start = system_clock::now();

        scriptEngine.gameLoop();
        renderPipeline->render(renderer);

        editor.loop();

        auto timeTaken = duration_cast<milliseconds>(system_clock::now() - start);
        sleepFor = timeToSleep - timeTaken;

        // Sleep between loops so that the game doesn't use up all your CPU power
        sleep_for(sleepFor);
    });

    scriptEngine.cleanUp();

    editor.shutdown();
    renderer.terminate();

    soundManager->shutdown();

    return 0;
}

#include <fstream>

namespace FileWriter
{
using std::string;

void writeFile(string fileName, string output)
{
    using std::ofstream;

    ofstream file;
    file.open(fileName, std::ofstream::out);

    file << output;

    file.close();
}
} // namespace FileWriter
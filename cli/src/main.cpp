#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <libgen.h>
#include <unistd.h>
#include <cstdlib>
#include <linux/limits.h>

std::filesystem::path get_current_path()
{
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);

    std::string path = "";

    if (count != -1) {
        path = std::string(dirname(result));
    }

    return std::filesystem::path(path);
}

// Hacky hard coded paths relative to cli install
const auto root_path = get_current_path()
    .parent_path()
    .parent_path();

void create_manifest(std::filesystem::path path, std::string name)
{
    std::string manifest_data = "GAME_NAME=" + name + "\n";

    std::ofstream manifest;
    manifest.open (path);
    manifest << manifest_data;
    manifest.close();
}

void create_vscode_settings(std::filesystem::path path)
{
    auto std_path = std::filesystem::path(root_path)
        .append("std")
        .append("src");

    std::string settings_data = "{"
        "\"Lua.workspace.library\": ["
            "\"" + std_path.string() + "\""
        "]"
    "}";

    std::ofstream manifest;
    manifest.open (path);
    manifest << settings_data;
    manifest.close();
}

void editor()
{
    std::cout << "Starting editor..." << std::endl;

    auto editor_path = std::filesystem::path(root_path)
        .append("install")
        .append("engine")
        .append("game_editor");
    auto editor_lib_path = std::filesystem::path(root_path)
        .append("editor");
    auto editor_script_path = std::filesystem::path(editor_lib_path)
        .append("scripts")
        .append("editor.lua");

    auto command = editor_path.string() + " " +
        editor_script_path.string() + " " + editor_lib_path.string();

    std::system(command.c_str());
}

void help()
{
    std::cout << "\n"
        << "=== Help ===\n"
        << "Get help (this): magic help\n"
        << "Create a new game: magic new name_of_game\n"
        << "Start the magic editor: magic editor\n"
        << "Upgrade the Magic game engine version: magic upgrade"
        << std::endl;
}

void new_game(int argc, char* argv[])
{
    using namespace std::filesystem;

    if (argc < 3)
    {
        std::cerr << "Missing name of game" << std::endl;
        return;
    }

    auto name = std::string(argv[2]);

    auto game_path = current_path().append(name);
    auto vscode_hidden_path = current_path().append(name).append(".vscode");

    if (exists(game_path))
    {
        std::cerr << game_path << " already exists" << std::endl;
        return;
    }

    create_directory(game_path);
    create_directory(vscode_hidden_path);

    copy_file(
        path(root_path)
            .append("install")
            .append("engine")
            .append("game"),
        path(game_path).append("game"));

    copy(
        path(root_path)
            .append("assets"),
        path(game_path).append("assets"),
        copy_options::recursive);

    create_manifest(
        path(game_path).append("manifest"),
        name
    );

    create_vscode_settings(vscode_hidden_path.append("settings.json"));

    std::cout << "Created new game at " << game_path << std::endl;
}

void upgrade()
{
    using namespace std::filesystem;

    auto game_path = current_path().append("game");
    auto lib_path = current_path().append("assets").append("lib");

    if (!exists(game_path) || !exists(game_path))
    {
        std::cerr << "This doesn't seem to be a magic game" << std::endl;
        return;
    }

    std::filesystem::remove(game_path);
    std::filesystem::remove(lib_path);

    copy_file(
        path(root_path)
            .append("install")
            .append("engine")
            .append("game"),
        game_path);

    copy_file(
        path(root_path)
            .append("assets")
            .append("lib"),
        lib_path);

    std::cout << "Upgraded Magic Game Version" << std::endl;
}

void unknown(std::string& command)
{
    std::cerr << "\n"
        << "Unknown command: " << command
        << std::endl;
    
    help();
}

int main(int argc, char* argv[])
{
    std::cout << "✨ Magic Game Engine ✨" << std::endl;

    if (argc < 2)
    {
        help();
        return 0;
    }

    auto command = std::string(argv[1]);

    if (command == "help")
    {
        help();
    }
    else if (command == "new")
    {
        new_game(argc, argv);
    }
    else if (command == "editor")
    {
        editor();
    }
    else if (command == "upgrade")
    {
        upgrade();
    }
    else
    {
        unknown(command);
    }

    return 0;
}
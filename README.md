# Magic Game Engine

This is a lua game engine that can build games for Linux and Mac OS.

## INSTALLATION

Simple run `make` to build the engine.

Then add the cli to your PATH. eg:
```bash
export PATH="/path/to/install/cli:$PATH"
```

Then use `magic` to create your new game!

eg:
```bash
magic new example_game
```

## Development

Run `make run` if you want to start the test game locally.

Run `make levelEditor` to launch the level editor.

Run `make test` to run all the tests. A git hook is set up when `make` is run which will auto configure
a pre-commit hook to run that will run all tests.

### Compilation

#### Common

Install `npm` for compiling the standard library.

#### Mac OS

To build for Mac OS, run `make install` and then `make build/macosx`.

**Warning:** The Mac OS version has not been built recently

#### Linux (Ubuntu)

To build for Linux, install the following prerequisites -
`sudo apt-get install libreadline-dev cmake gcc-8 g++-8 libpthread-stubs0-dev xorg-dev libglu1-mesa-dev libglew-dev`

**Note:** gcc-8 is expected as the default compiler

## PROJECT STRUCTURE
- `test`            - This is where all the tests are located
- `engine`          - This is where all the core engine logic sits
- `std`             - This is where the standard library is stored
- `cli`             - This is the cli project to quickly create new games and launch the editor
- `assets`          - This is where all the game assets are including the script code for the levels
    - `levels`           - This is where the level code is stored for games
    - `scripts`          - This is the game specific logic

## TODOS
[ ] Investigate adding sqlite for components